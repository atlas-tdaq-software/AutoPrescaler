//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
//------------------------------------------------------------------------------


L1CT::ISServer L1CT::ISServer::initialL1CT("initial", "L1CT");
L1CT::ISServer L1CT::ISServer::ATLAS("ATLAS", "L1CT");
L1CT::ISServer L1CT::ISServer::PerBunchHistograms("initialL1CT", "L1CTPerBunch");
