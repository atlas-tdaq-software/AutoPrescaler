//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/PrescalerIpcCommander.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
bool PrescalerIpcCommander::requestPrescale(const unsigned long psk, const MenuDraft::Draft_t &draft) {
    ERS_INFO("Sending a request to Prescaler");
    L1CTIPC::PrescaleRequest request;
    request.psk = psk;
    request.values.length(draft.size());

    size_t i = 0;
    for (const auto &it : draft) {
        request.values[i].item      = it.first.c_str(); 
        request.values[i].prescale  = it.second;

        ERS_INFO("Saved " << request.values[i].item << " " << request.values[i].prescale);

        ++i;
    }

    prescaler()->requestPrescale(request);

    return true;
}
//------------------------------------------------------------------------------

