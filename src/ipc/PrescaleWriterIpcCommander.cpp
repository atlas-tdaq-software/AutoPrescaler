//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/PrescaleWriterIpcCommander.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------




//------------------------------------------------------------------------------
int PrescaleWriterIpcCommander::createPrescale(const unsigned long psk, const PrescaleBuffer &prescales) {
    ERS_INFO("Sending a request to AutoPrescaleEditor");
    L1CTIPC::PrescaleRequest request;
    request.psk = psk;
    request.values.length(prescales.size());
    
    size_t i = 0;
    for (const auto &it : prescales) {
        request.values[i].item      = it.triggerName.c_str();
        request.values[i].prescale  = it.prescale;

        ERS_INFO("Saved " << request.values[i].item << " " << request.values[i].prescale);

        ++i;
    }

    L1CTIPC::PrescaleReturn myResult;
    try {
      myResult = prescaler()->requestPrescale(request);
    } catch (...) {
      return -1;
    }    

    ERS_INFO("Received response from AutoPrescaleEditor: " << (myResult.success == true ? "success" : "failure") << ", psk " << myResult.psk);

    if (!myResult.success) return -1;
    
    return int(myResult.psk); // it is unsigned long
}
//------------------------------------------------------------------------------

