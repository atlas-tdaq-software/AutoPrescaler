//------------------------------------------------------------------------------
#include<AutoPrescaler/ipc/PrescalerIpc.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
::CORBA::Boolean PrescalerIpc::requestPrescale(const L1CTIPC::PrescaleRequest& request) {
    ERS_INFO("Received a request from RuleChecker");
    const size_t N = request.values.length();
    const boost::posix_time::ptime currentTime = boost::posix_time::microsec_clock::local_time(); //now();

    std::vector<PrescaleBuffer::PrescaleEntry> buffer(N);

    for(size_t i = 0; i < N; ++i) {
        auto &entry    = buffer[i];
        auto &prescale = request.values[i];

        entry.timeStamp     = currentTime;
        entry.prescaleKey   = request.psk;
        entry.triggerName   = prescale.item;
        entry.prescale      = prescale.prescale;

        ERS_INFO("    " << entry.triggerName << " " << entry.prescale);
    }

    return prescaler().bufferRequests(buffer);
}
//------------------------------------------------------------------------------


