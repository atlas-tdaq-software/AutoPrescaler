//------------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include "rc/RunParams.h"
#include "rc/RCStateInfo.h"
//------------------------------------------------------------------------------
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/partition.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
#include <AutoPrescaler/prescaler/Prescaler.h>
#include <AutoPrescaler/ipc/PrescalerIpc.h>
//------------------------------------------------------------------------------
#include "AutoPrescaler/prescaler/PrescalerWrapper.h"
//------------------------------------------------------------------------------


// ------------------ IMPLEMENTATION -----------------

using namespace std;
using namespace L1CT;

/** \brief Constructor: initialise variables
 */
PrescalerWrapper::PrescalerWrapper() : daq::rc::Controllable()
{
  ERS_LOG(" *** Start of PrescalerWrapper::PrescalerWrapper() ***");
}

/** \brief Get application configuration from OKS.
 */
void PrescalerWrapper::configure(const daq::rc::TransitionCmd& )
{
  ERS_LOG(" *** Start of PrescalerWrapper::configure() ***");
  AP_ERS_DEBUG("Test the debug level");
  
  daq::rc::OnlineServices& services = daq::rc::OnlineServices::instance();

  // Initialiase handle to IS
  services.getIPCPartition(); //I don't think this and like 37 is needed....
  const IPCPartition part(daq::rc::OnlineServices::instance().getIPCPartition());
  m_isDict = new ISInfoDictionary(part);

  ERS_LOG(" *** Reading in configuration: ***");  
  
  const std::string partitionNameInp = static_cast<std::string>(m_partName);
  if (partitionNameInp != ISServer::ATLAS.PartitionName) {
    ISServer::ATLAS.redefine(partitionNameInp, ISServer::ATLAS.ServerName);
  }
  
  // Fix this later
  //const std::string partitionNameInp = static_cast<std::string>(ISServer::ATLAS.PartitionName);

  m_prescaler = new Prescaler();

  //const std::string ipcName = getenv("TDAQ_APPLICATION_NAME") == nullptr ? "CtpAutoPrescaler" : getenv("TDAQ_APPLICATION_NAME");
  const std::string ipcName = "CtpAutoPrescaler";
  m_prescalerIpc = new PrescalerIpc(ipcName, *m_prescaler);

  ERS_LOG("Setting log-level to " << int(m_verbose));
  ERS_LOG(" *** End of PrescalerWrapper::configure() ***");
}

/** \brief Run is about to start: start timer. Time interval should be set to max LB duration / 1.2
 */
void PrescalerWrapper::prepareForRun(const daq::rc::TransitionCmd& )
{
}

/** \brief Run was just stopped: stop timer and clear buffers. We're done.
 */
void PrescalerWrapper::stopArchiving(const daq::rc::TransitionCmd& )
{
}

/** \brief Publish() is called regularly by RC. PrescalerWrapper checks are issued from here.
 *  Additionally the frequency of the calls of Publish() is monitored and compared to a dedicated OKS 
 * parameter. In case publish is called to rarely WARNINGS are issued.
 */
void PrescalerWrapper::publish()
{
  // check if root controller is in running state
  RCStateInfo rcStateInfo;
  m_isDict->getValue("RunCtrl.RootController", rcStateInfo);
  if (rcStateInfo.state != "RUNNING") {
    ERS_LOG(" *** PrescalerWrapper - Trying to publish, but RC state is not RUNNING ***");
    return;
  }
  
  m_prescaler->processRequests();
}


