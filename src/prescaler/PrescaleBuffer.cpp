//------------------------------------------------------------------------------
#include <AutoPrescaler/prescaler/PrescaleBuffer.h>
//------------------------------------------------------------------------------
#include <map>


//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
void PrescaleBuffer::mergeBuffer()
{
  // Depends how smart we want to be here, but for now we assume
  // that entries are time-ordered
  
  std::map<std::string, PrescaleEntry> tempMap;

  while(m_contents.size() > 0)
    {
      // Pop-Back last entry
      PrescaleEntry myEntry = m_contents.back();
      m_contents.pop_back();
      
      // If it's not in the map, insert it
      // We can add extra safety later
      if(tempMap.find(myEntry.triggerName) == tempMap.end())
	{
	  tempMap[myEntry.triggerName] = myEntry;
	}
    }
  
  // Re-fill the current vector with only the map's contents
  // Ordering does not matter much anymore
  m_contents.clear();
  for(auto entry: tempMap)
    {
      m_contents.push_back(entry.second);
    }
}
//------------------------------------------------------------------------------

void PrescaleBuffer::rejoinBuffers(PrescaleBuffer& myBuffer)
{
  while(myBuffer.size() > 0) {
    push_front(myBuffer.back());
    myBuffer.pop_back();
  }
}
