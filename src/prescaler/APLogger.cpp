
#include <AutoPrescaler/prescaler/APLogger.h>


// Initialize to default level
//APLoggerStatus::LoggingLevel APLoggerStatus::m_level = APLoggerStatus::LoggingLevel::INFO;


void APLoggerStatus::setLevel(APLoggerStatus::LoggingLevel level) {

    switch (level) {
        case APLoggerStatus::LoggingLevel::INFO:
            ERS_INFO ("AP logging level will be set to INFO - DEBUG off!");
            break;
        case APLoggerStatus::LoggingLevel::DEBUG:
            ERS_INFO ("AP logging level will be set to DEBUG");
            break;
        default:
            ERS_INFO("Logging level not recognized! DEBUG - 2, INFO - 1");
            return;
    }

    APLoggerStatus::m_level = level;
}


void APLoggerStatus::setLevel(unsigned int level) {
    APLoggerStatus::setLevel(static_cast<APLoggerStatus::LoggingLevel>(level));
}


APLoggerStatus::LoggingLevel APLoggerStatus::getLevel() {
    return m_level;
}

