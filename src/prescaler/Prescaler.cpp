//------------------------------------------------------------------------------
#include <AutoPrescaler/prescaler/Prescaler.h>
#include <AutoPrescaler/ipc/PrescaleWriterIpcCommander.h>
#include <AutoPrescalerControllerMasterTrigger/AutoPrescalerControllerMasterTrigger.hh>
//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
#include <TTCInfo/CtpControllerL1PSKeyList.h>
#include <TTCInfo/CtpControllerL1PSKey.h>
#include <TriggerCommander/TriggerCommander.h>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------
#ifndef ERS_WARNING
#define ERS_WARNING(X) do { std::cout << X << std::endl; } while(false)
#endif
#ifndef ERS_ERROR
#define ERS_ERROR(X) do { std::cerr << X << std::endl; } while(false)
#endif
//------------------------------------------------------------------------------
Prescaler::Prescaler()
  : m_init(false), m_validPSK(0) {
  // Set logger name here...

  //  log.prefix = "Prescaler";

  // Subscribe to the IS publications we want
  subscribe();
}

//------------------------------------------------------------------------------
Prescaler::~Prescaler() {}
//------------------------------------------------------------------------------

// Specialization of subscribe: Specific IS entries we care about for this
//------------------------------------------------------------------------------
void Prescaler::subscribe() {

  if (m_subscribed) return;

  // Subscribe here... To anything in this case.
  bool success = false;
  // We temporarily add some protection to the subscription
  while(1) {
    ERS_INFO("Attempting to subscribe to L1CT: ");
    try{
      subscribeToPrimary("L1CT", "CtpControllerLoadedL1PSKey");
      m_infoSubscribed["L1CT.CtpControllerLoadedL1PSKey"] = &m_L1PSK;
      ERS_INFO("Subscription successful!");
      success = true;
    } catch(...) {
      ERS_INFO("Failed to subscribe to L1CT");
      ERS_INFO("Will attempt again in 10 seconds");
      sleep(10);
    }

    if (success) break;
  }
  
  m_subscribed = true;
  ERS_INFO("If restarting: try to get published values");
  try {
    auto & dict = ISServer::ATLAS.dictionary();
    
    dict.getValue("RunParams.RunParams", m_runParams);
    ERS_INFO("Read in the current RunParams: " << m_runParams);    

    dict.getValue("L1CT.CtpControllerLoadedL1PSKey", m_L1PSK);
    ERS_INFO("Read in the current PSK: " << m_L1PSK);
    
    m_listPSK.push_back(m_L1PSK.L1PrescaleKey);
    publishToIS();

    m_init = true;
  } catch(...) {
    ERS_INFO("Values not published yet: wait for callback instead");
  }

}
//------------------------------------------------------------------------------

// Publish to IS
//------------------------------------------------------------------------------
void Prescaler::publishToIS() {
  try {
    CtpControllerL1PSKeyList is_entry;
    std::vector<CtpControllerL1PSKey> is_entry_items;

    // Format properly for IS - we can be smarter about this
    for (auto key : m_listPSK) {
      CtpControllerL1PSKey myKey;
      myKey.L1PrescaleKey = key;
      is_entry_items.push_back(myKey);
    }
    is_entry.items = is_entry_items;

    // Attempt to publish to IS
    auto & dict = ISServer::ATLAS.dictionary();
    dict.checkin("L1CT.AutoPrescaler.PSKHistory", is_entry, false); // requirement: publish without history or you might read back a wrong entry for the next run
    ERS_LOG("Prescales list was published to IS successfully");

    // catch exceptions
  } catch (CORBA::SystemException& ex) {
    ERS_ERROR("Something went wrong in IS publication: " << ex._name());    
  } catch (daq::is::Exception & ex) {
    ERS_ERROR("Something went wrong in IS publication: " << ex.what());    
  } catch (std::exception& ex) {
    ERS_ERROR("Something went wrong in IS publication: " << ex.what());    
  } catch (...) {
    ERS_ERROR("Something went wrong in IS publication: Unknown exception.");    
  }
}
//------------------------------------------------------------------------------

// Receive requests for prescales
//------------------------------------------------------------------------------
bool Prescaler::bufferRequests(const std::vector<PrescaleBuffer::PrescaleEntry> &buffer) {
  // Prevent requests from interfering with each other, ensure none are lost
  boost::recursive_mutex::scoped_lock lock(m_mutex);
  AP_ERS_DEBUG("Buffering " <<  buffer.size() << " requests");
  for (auto &e : buffer) {
    m_buffer.addEntry(e);
  }

  return true;
}
//------------------------------------------------------------------------------


// Check if any new requests should be sent
//------------------------------------------------------------------------------
bool Prescaler::processRequests() {
  ERS_LOG("Processing buffered prescale request");
  
  // We don't want to process a request while we're getting a callback...
  boost::recursive_mutex::scoped_lock lock(m_callbackLock);

  // Only proceed if we're 100% convinced we initialized properly
  if (!m_init) {
    ERS_WARNING("Still missing a PSK: initialization is not yet completed");
    return true;
  }

  // In case, we have no PSK read in, exit for safety
  // Do not merge the buffer
  if (m_listPSK.size() == 0) {
    return true;
  }

  // Safety check: if the master trigger hasn't loaded our pre-scale key,
  // gently remind it to do so :)
  if (m_listPSK.back() != m_L1PSK.L1PrescaleKey) {
    boost::recursive_mutex::scoped_lock lock(m_prescaleLock);
    ERS_INFO("Prescale key: " << m_listPSK.back() << " not used yet, using: " << m_L1PSK.L1PrescaleKey);
    ERS_INFO("Request PSK Change to CtpController: " << m_listPSK.back());   
    std::string controllerName("CtpController"); // hard-code?
    daq::trigger::TriggerCommander commander(ISServer::ATLAS.partition(), controllerName);
    ::L1CTIPC::AutoPrescalerControllerMasterTrigger_var controller;

    try {
      controller = ISServer::ATLAS.partition().lookup < ::L1CTIPC::AutoPrescalerControllerMasterTrigger > (controllerName.c_str());
      // New accessor
      controller->setL1PrescalesByAutoPrescaler(m_listPSK.back());
      // Old accessor
      //commander.setL1Prescales(m_listPSK.back());
      // Increment the lumi block
      auto & dict = ISServer::ATLAS.dictionary();
      dict.getValue("RunParams.RunParams", m_runParams);
      ERS_LOG("Read in the current RunParams: " << m_runParams);
      
      commander.increaseLumiBlock(m_runParams.run_number);
    } catch(...) {
      ERS_ERROR("Prescale change refused by CtpController!");     
    }
    
  }

  PrescaleBuffer lastBuffer;
  PrescaleBuffer request;

  {
    // Make sure we don't accept a request while clearing the buffer
    // Only the operations which need the buffer are scoped here
    boost::recursive_mutex::scoped_lock lock(m_mutex);
    
    // Copy the buffer, clear it, release it so it can accept new requests
    lastBuffer = m_buffer;
    m_buffer.clearBuffer();
  }

  // Merge buffer first
  lastBuffer.mergeBuffer();

  // Check validity of specific requests
  for (const PrescaleBuffer::PrescaleEntry& entry: lastBuffer){
    bool isValid = false;
    AP_ERS_DEBUG("Prescale key validity check: " << entry.prescaleKey);
    for (unsigned int key: m_listPSK) {
      AP_ERS_DEBUG("LocalKey: " << key);
      if (key == entry.prescaleKey){
        isValid = true;
      }
    }

    // Check whether the PS request is redundant - if so, wipe it.
    // This is incredibly messy, and should be fixed later...
    CtpcoreTriggerRateInfo myRates;
    auto & dict = ISServer::ATLAS.dictionary();
    dict.getValue("L1CT.CTPCORE.Instantaneous.TriggerRates", myRates);

    for (const CtpcoreItemRateInfo& item : myRates.items) {
      if (item.name == entry.triggerName) {
        float ratio = item.prescale;
        if (item.tap > 0) {
          ratio = item.tbp/item.tap;
        }

        if ((ratio > 0) && (fabs(ratio-entry.prescale)/ratio < 0.3) ) {
          ERS_INFO("Prescale " << entry.prescale << " for " << entry.triggerName << " is within 30% of an existing one " << ratio << ". Rejecting! ");
          isValid = false;
        }
      }
    }
      
    // Here we can prepare a valid new PSK
    if (isValid) {
      ERS_INFO("Prescale Request Validated for item " << entry.triggerName);
      ERS_INFO("    PSK Validity: " << entry.prescaleKey);
      ERS_INFO("    PSK Value: " << entry.prescale);
      ERS_INFO("    Timestamp: " << entry.timeStamp);

      request.addEntry(entry);
    } else {
      ERS_WARNING("Prescale Request for item " << entry.triggerName << " is not valid");
    }
  }

  // Request is empty: we're done.
  if (request.size() == 0){
    ERS_INFO("The prescale request is empty.");
    return true;
  }
  
  {
    // We have a valid request: prepare new prescale key
    // Scoped to prevent requesting multiple L1PSKs at once
    boost::recursive_mutex::scoped_lock lock(m_prescaleLock);
    PrescaleWriterIpcCommander prescaleWriter;
    
    // This would be ideal: Start from the last PSK we made...
    // However this doesn't work well with usermade resets
    //int generatedPSK = prescaleWriter.createPrescale(m_listPSK.back(), request);

    // Instead, use the loaded key - this is to deal with cases when the user
    // reset to the base PSK by hand - we'd otherwise get trapped.
    int generatedPSK = prescaleWriter.createPrescale(m_listPSK.back(), request);
    
    ERS_INFO("Generated Prescale Key " << generatedPSK);
    
    // At this point, if anything fails, we re-append the contents to the old buffer.
    if (generatedPSK == -1) {      
      ERS_INFO("Prescale Key Generation Failed: Try again at the next iteration");
      boost::recursive_mutex::scoped_lock lock(m_mutex);
      m_buffer.rejoinBuffers(lastBuffer);
      return true;
    }
    
    // Request PSK change - what is the expected delay?
    ERS_INFO("Request PSK Change to " << generatedPSK << " to CtpController");
    std::string controllerName("CtpController"); // hard-code?
    daq::trigger::TriggerCommander commander(ISServer::ATLAS.partition(), controllerName);
    ::L1CTIPC::AutoPrescalerControllerMasterTrigger_var controller;
    
    try {
      controller = ISServer::ATLAS.partition().lookup < ::L1CTIPC::AutoPrescalerControllerMasterTrigger > (controllerName.c_str());
      controller->setL1PrescalesByAutoPrescaler(generatedPSK);
      AP_ERS_DEBUG("Sent new L1 psk key " << generatedPSK << " to Trigger Commander");

      // Increment the lumi block
      auto & dict = ISServer::ATLAS.dictionary();
      dict.getValue("RunParams.RunParams", m_runParams);
      ERS_LOG("Read in the current RunParams: " << m_runParams);

      commander.increaseLumiBlock(m_runParams.run_number);
    } catch(...) {
      ERS_WARNING("Prescale change refused by CtpController!");
    }
    
    // Update internal PSK book-keeping
    {
      boost::recursive_mutex::scoped_lock lock(m_callbackLock);
      m_listPSK.push_back(generatedPSK);
    }

    // Publish internal PSK book-keeping
    publishToIS();    
  }
  
  return true;
}

void Prescaler::update(ISCallbackInfo * isc) {
  // Prevent multiple updates at the same time
  boost::recursive_mutex::scoped_lock lock(m_callbackLock);

  // Update IS info and whatever else needs to be done. Internal flags etc.
  const std::string name  = isc->name();
  const auto type         = isc->type();

  AP_ERS_DEBUG("Received IS callback from " << name << " type: " << type << " size: " << m_infoSubscribed.size());

  // TODO - check if this printout is already somewhere
  for (auto entry: m_infoSubscribed) {
    AP_ERS_DEBUG(entry.first);
  }

  // Generic approach: update the map with whatever we got a callback from
  infomap_t::iterator entry = m_infoSubscribed.find(name);
  if (entry != m_infoSubscribed.end() ) {
      
    isc->value(*(m_infoSubscribed[name]));
    // Special treatment for potential L1CT key changes

    if (name == "L1CT.CtpControllerLoadedL1PSKey") {
      ERS_LOG("Current PSK read from L1CT.CtpControllerLoadedL1PSKey is " << m_L1PSK.L1PrescaleKey);

      bool inList = false;
      for (auto key : m_listPSK ){
        // Expected PSK change - all good, carry on
        if (key == m_L1PSK.L1PrescaleKey){
          inList = true;
        }
      }

      // Wipe and start again (!!)
      if (!inList) {
        m_listPSK.clear();
      }
    
      // Handle both initialization and overwrite
      if (m_listPSK.size() == 0) {
        m_listPSK.push_back(m_L1PSK.L1PrescaleKey);	    
        publishToIS();
        m_init = true;
      }
    }  
    return;
  }
}
