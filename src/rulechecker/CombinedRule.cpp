#include <AutoPrescaler/rulechecker/CombinedRule.h>
#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>

//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
int CombinedRule::powerOf(float targetPrescale) {
  int prescale = 1;
  if (m_roundingFactor == 1) return prescale;
  while (prescale < targetPrescale) {
    prescale*=m_roundingFactor;
  }

  return prescale;
}


bool CombinedRule::request() {

  // We need to consider all possibilities for state transitions here
  bool makeRequest = false;
  
  // Temporary book-keeping for the current state
  bool tempHot = false;
  bool tempBeam = false;
  bool tempCold = false;
  bool tempNotBeam = false;

  // Begin by extracting information from the rate history
  // Is the entire history above/below the maxRate?
  bool allAbove = true;
  // entire rate history with original prescale below max rate
  bool allBelow = true;
  // entire rate history with prescale of one below max rate
  bool allBelowUnprescaledRate = true;
  
  float largestPrescaledRate = 0;
  float largestOriginalRate = 0;
  float largestUnprescaledRate = 0; // this value is not really needed

  // Extract current prescale
  float currentPS = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);
  float targetPrescale = currentPS;
  float requestedPS = targetPrescale; //int(targetPrescale+0.5); //TODO remove

  std::string logMsg;

  logMsg.append("Preparing for CombinedRule action. Current Status:");
  if (m_isHot) logMsg.append("We are already in hot item mode");  
  if (m_isBeam) logMsg.append("We are already in beam mode");    
  if (!m_isBeam && !m_isHot) logMsg.append("We are in a neutral state");

  if (!logMsg.empty()) {
    AP_ERS_DEBUG(logMsg);
    logMsg.clear();
  }

  // Look through the prescaled rates: are we hot?
  for (const auto& rate : m_prescaledRates) {
    if (rate.first > largestPrescaledRate){
      largestPrescaledRate = rate.first;
    }
    
    if (rate.first <= m_maxRate){
      allAbove = false;
    }
  }

  // Look through the rates, using the original prescale: would we still be hot?
  for (const auto& rate : m_originalRates ) {
    if (rate.first > largestOriginalRate){
      largestOriginalRate = rate.first;
    }
      
    if (rate.first > m_maxRate){
      allBelow = false;
    }
  }

  for (const auto& rate : m_unprescaledRates){
    if (rate.first > largestUnprescaledRate){
      largestUnprescaledRate = rate.first;
    }
    
    if(rate.first > m_maxRate){
      allBelowUnprescaledRate = false;
    }
  }

  // Probably too verbose
  AP_ERS_DEBUG("    Max allowed rate is: " << m_maxRate << ", Target rate is: " << m_targetRate);
  AP_ERS_DEBUG("    Largest rate is (prescaled): " << largestPrescaledRate);
  AP_ERS_DEBUG("    Largest rate is (original prescale): " << largestOriginalRate);
  AP_ERS_DEBUG("    Largest rate is (unprescaled): " << largestUnprescaledRate);

  if (allAbove) logMsg.append("We are going hotter. ");
  if (allBelow) logMsg.append("We are cold. ");
  if (allBelowUnprescaledRate) logMsg.append("We can unprescale the item entirely. ");

  if (!logMsg.empty()) {
    ERS_LOG(logMsg);
    logMsg.clear();
  }
  
  bool applyHotPrescale = false;
  // Cold -> Hot
  if(!m_isHot && m_timeElapsed && allAbove)
    applyHotPrescale = true;

  // Hot -> Hotter (only if a full integration loop has gone by)
  // Need a finer-tuned logic here...
  boost::posix_time::time_duration diff = boost::posix_time::microsec_clock::local_time() - m_lastHot;
  float timeElapsed = diff.total_microseconds()/1000000.;

  if (m_isHot && m_timeElapsed && allAbove && timeElapsed > m_integrationTime)
    applyHotPrescale = true;
  
  // Any transition that requires pre-scaling
  if (applyHotPrescale) {

    // Now, we look for the target rate, keeping in mind the safety factor
    targetPrescale *= (largestPrescaledRate / float(m_targetRate));
    logMsg.append("Target pre-scale is: " + std::to_string(targetPrescale) + " ");

    // Round to nearest power of 2, but can eventually use something much smarter...
    requestedPS = powerOf(targetPrescale);

    // This is useful to check we've been hot long enough
    m_lastHot = boost::posix_time::microsec_clock::local_time();      
    tempHot = true;
    makeRequest = true;
    //m_newPS = false;
  } 

  
  // Hot -> Cold
  if (m_isHot && allBelow) {
      // We are not in beamMode: relax entirely by reverting pre-scale - simple as that
      if (!m_isBeam) {
        logMsg.append("Going cold, back to original PS: " + std::to_string(m_originalPS) + " ");
        
        requestedPS = m_originalPS; //int(m_originalPS+0.5); //TODO remove
        tempCold = true;
        makeRequest = true;
      }
      // We are in a beamMode
      else {
        // We need to check that we can handle a PS of 1
        // Otherwise, we stay in hot item mode!
        if (allBelowUnprescaledRate) {
            logMsg.append("Going cold, back to beamMode PS of 1 ");

            requestedPS = 1;
            tempCold = true;
            makeRequest = true;
          }
      }
    }

  // Hot -> Still Hot: nothing happens - we carry on living happily

  // Now handle the beamMode cases (!!!)
  std::string beamWord = rulechecker().dataModel().abstraction().getBeamMode();

  bool goodMode = false;
  AP_ERS_DEBUG("Current Beam Mode: " << beamWord);

  for (const std::string& beamMode : m_beamModes) {
    if (beamWord == beamMode){
      goodMode = true;
      AP_ERS_DEBUG("Beam Mode matches requirements");
      break;
    }
  }

  // Entering beamMode
  if (!m_isBeam && goodMode) {
    // We unprescale the item - but we need to be careful
    // If the item is currently hot, there's nothing we can do.  Just wait
    // for the next iteration
    bool currentlyHot = m_isHot || tempHot;
    
    // If we are not hot, we can un-prescale the item, but...
    if (!currentlyHot) {
      // Can we handle the rate?
      if (largestUnprescaledRate > m_maxRate) {
        // If we can't handle the rate, we design a safe 'beamPrescale'
        // though we don't declare ourselves hot (that could lead to disasters)
        // This safe beamPrescale will remain there for the entire period

        // Now, we look for the target rate, keeping in mind the safety factor
        targetPrescale = (largestUnprescaledRate / float(m_targetRate));
        logMsg.append("Beam Mode: Unprescale with: " + std::to_string(targetPrescale) );	      

        // Round to nearest power of 2, but can eventually use something much smarter...
        requestedPS = powerOf(targetPrescale);
        makeRequest = true;
        tempBeam = true;
      }
      else {
        // Phew, finally a simple case.  Unprescaaaale!
        logMsg.append("Beam Mode: Unprescale fully");
        requestedPS = 1;
        tempBeam = true;
        makeRequest = true;
      }
    }
  }
  
  // Leaving beamMode
  if (m_isBeam && !goodMode){
    // Return to original prescale
    // No real need for safety here
    logMsg.append("Leaving Beam Mode: Reverting to original prescale of: " + std::to_string(m_originalPS));

    requestedPS = m_originalPS; //int(m_originalPS+0.5);
    tempCold = true;
    tempNotBeam = true;
    makeRequest = true;
  }

  if (!logMsg.empty()) {
    ERS_LOG(logMsg);
    logMsg.clear();
  }

  if (timeElapsed <= m_integrationTime) {
    ERS_LOG( "The time since the item was detected as hot is  " << timeElapsed << " s, the integration time is " << m_integrationTime << " s. Request will not be made." );
  }
  
  // Request menu update, no matter what (this is a bit much, but it works fine)
  if (makeRequest) {
    ERS_LOG("Making a request for: " << requestedPS);
    rulechecker().requestDraft(m_itemName, requestedPS);
  } else {
    AP_ERS_DEBUG("No requests made..."); 
  }
    
  // We are done: trigger state transition
  if (tempHot) m_isHot = true;
  if (tempCold) m_isHot = false;

  if (tempBeam) m_isBeam = true;
  if (tempNotBeam) m_isBeam = false;

  return true;
}

//------------------------------------------------------------------------------
bool CombinedRule::update() {

  // Check if the monitored item is present: probably want a smarter protection
  if (!rulechecker().dataModel().abstraction().checkItem(m_itemName)){
    AP_ERS_DEBUG("Item " << m_itemName << " is not in the abstraction");
    return false;
  }

  // If item is disabled, we just idle
  if (!rulechecker().dataModel().abstraction().isEnabled(m_itemName)){
    AP_ERS_DEBUG("Item " <<  m_itemName << " is disabled");
    reset();
    return false;
  }

  float currentPS = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);

  // Store the starting prescale
  if (!m_initialized){
    m_originalPS = currentPS;
    m_initialized = true;
  }  

  // Some ugly placeholder logic here
  float currentRateP = rulechecker().dataModel().abstraction().getL1Rate(m_itemName, L1CT::DataAbstraction::L1Item::Type::TAP);
  float currentRateU = rulechecker().dataModel().abstraction().getL1Rate(m_itemName, L1CT::DataAbstraction::L1Item::Type::TBP);

  // Book-keeping via FIFO
  boost::posix_time::ptime thisTime = boost::posix_time::microsec_clock::local_time();
  std::pair<float, boost::posix_time::ptime> currentEntryU(currentRateU, thisTime);
  std::pair<float, boost::posix_time::ptime> currentEntryO(currentRateU/m_originalPS, thisTime);
  std::pair<float, boost::posix_time::ptime> currentEntryP(currentRateP, thisTime);
  std::pair<int, boost::posix_time::ptime> currentEntryPS(int(currentPS+0.5), thisTime);

  m_unprescaledRates.push_back(currentEntryU);
  m_originalRates.push_back(currentEntryO);
  m_prescaledRates.push_back(currentEntryP);
  m_prescales.push_back(currentEntryPS);

  ERS_LOG("Item " <<  m_itemName << " filling with 3 rate entries: before prescale (unprescaled) " << currentEntryU.first << " original prescale " 
          << currentEntryO.first << " after prescale (from IS)" << currentEntryP.first << " and prescale " << currentEntryPS.first);
  AP_ERS_DEBUG("Number of entries in prescale array: " << m_prescales.size());

  // remove entries where time elapsed was too long ago
  if (m_prescales.size() > 0) {
    while(1) {
      boost::posix_time::time_duration diff = m_unprescaledRates.back().second - m_unprescaledRates.front().second;
      float timeElapsed = diff.total_seconds();
      if (timeElapsed > m_integrationTime){
        // First time we remove an entry, we are assured that the
        // minimum integration time has been fulfilled
        m_timeElapsed = true;
        m_unprescaledRates.pop_front();
        m_prescaledRates.pop_front();
        m_originalRates.pop_front();
        m_prescales.pop_front();
      }
      else {
        break;
      }  
    }
  }

  /// Detect whether we've got a prescale change in there
  // Wipe the store if that's the case - should make things cleaner now

  if (m_prescales.size() > 1){
    int lastPS = m_prescales.back().first;
    while (1) {

      // If we only have one entry, it makes no sense to try to wipe the array
      if (m_prescales.size() < 2) {
        break;
      }
      
      // Clear entries that had been recorded with a different pre-scale
      if (m_prescales.front().first != lastPS) {

        m_unprescaledRates.pop_front();
        m_prescaledRates.pop_front();
        m_originalRates.pop_front();
        m_prescales.pop_front();
        m_timeElapsed = false;
        
      } else {
        break;
      }
    }
  }
  // We're done updating the internal stores
  
  return true;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
std::vector<std::string> CombinedRule::needsItems() {
  std::vector<std::string> theItems;
  theItems.push_back(m_itemName); 
  
  return theItems;
}
//------------------------------------------------------------------------------


