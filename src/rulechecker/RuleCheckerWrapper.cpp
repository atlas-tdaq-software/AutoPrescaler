//------------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include <string>
#include <cstdlib> //std::getenv

#include <config/Configuration.h>

#include <AutoPrescaler/ipc/ISServer.h>
#include <APRules/AutoPrescalerRule.h>
#include <APRules/AutoPrescalerRuleList.h>
#include <AutoPrescaler/rulechecker/RuleCheckerWrapper.h>
#include <AutoPrescaler/rulechecker/RuleCheckerWrapper.hpp>

#ifndef ERS_ERROR
#define ERS_ERROR(X) do { std::cerr << X << std::endl; } while(false)
#endif


// brief Constructor: initialise variables  
RuleCheckerWrapper::RuleCheckerWrapper() : daq::rc::Controllable() {
  ERS_LOG(" *** Start of RuleCheckerWrapper::RuleCheckerWrapper() ***");
}

// brief Get application configuration from OKS
void RuleCheckerWrapper::configure(const daq::rc::TransitionCmd& ) {
  ERS_LOG(" *** Start of RuleCheckerWrapper::configure() ***");
  ERS_LOG(" *** Reading in configuration: ***");  
  
  const std::string envVar = "TDAQ_DB_DATA";
  std::unique_ptr<Configuration> oksHandle = nullptr;
  if (std::getenv(envVar.c_str())) {
    const std::string xml = std::getenv(envVar.c_str());
    try {
      oksHandle.reset(new Configuration("oksconfig:" + xml));
      ERS_INFO("Loaded configuration from file " << xml);  
    } catch (std::exception &e) {
      std::stringstream ss; ss << "Caught exception while reading XML: " << e.what();
      ERS_ERROR(ss.str());
      return;
    }

  } else {
    ERS_ERROR("Can't read XML directly: " << envVar << " not set.");
    return;
  }
  
  ERS_LOG("Configuration read properly.  Starting to configure...");

  std::vector<ConfigObject> objs;
  oksHandle->get("AutoPrescalerRuleList", objs);

  ERS_LOG("RuleList is loaded");
  if (objs.size() != 1) {
    ERS_ERROR("Found " << char(objs.size()) << " instances of AutoPrescalerRuleList, expected one.");
    return;
  }

  const APRules::AutoPrescalerRuleList* ruleListConf = oksHandle->get<APRules::AutoPrescalerRuleList>(objs[0]);
  const std::vector<const APRules::AutoPrescalerRule*>& ruleList = ruleListConf->get_ListOfRules();
  if (ruleList.size() == 0) {
    ERS_INFO("Rule list for application contains no rules.");
    return;
  }

  const std::string partitionNameInp = static_cast<std::string>(m_partName);
  if (partitionNameInp != L1CT::ISServer::ATLAS.PartitionName) {
    ERS_INFO("Re-defining partition name.");
    L1CT::ISServer::ATLAS.redefine(partitionNameInp, L1CT::ISServer::ATLAS.ServerName);
  }
  
  // use the initialL1CT to simply mean "initial" partition
  ERS_LOG("Creating RuleChecker and adding the rules...");  
  m_rulechecker = new L1CT::RuleChecker();

  std::stringstream stringstream_rule;
  for (const APRules::AutoPrescalerRule* rule : ruleList) {                                                                                                                     
    addRule(rule); // Defined in hpp file
  }

  ERS_LOG(" *** End of RuleCheckerWrapper::configure() ***");
  return;
}

/** \brief Run is about to start: start timer. Time interval should be set to max LB duration / 1.2
 */
void RuleCheckerWrapper::prepareForRun(const daq::rc::TransitionCmd& ) {}

/** \brief Run was just stopped: stop timer and clear buffers. We're done.
 */
void RuleCheckerWrapper::stopArchiving(const daq::rc::TransitionCmd& ) {}

/** \brief Publish() is called regularly by RC. RuleCheckerWrapper checks are issued from here.
 *  Additionally the frequency of the calls of Publish() is monitored and compared to a dedicated OKS 
 * parameter. In case publish is called to rarely WARNINGS are issued.
 */
void RuleCheckerWrapper::publish() {}


//------------------------------------------------------------------------------
