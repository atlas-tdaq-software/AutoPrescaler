#include <AutoPrescaler/rulechecker/BeamModeAction.h>
#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>

//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------

void BeamModeAction::print() {
  // Debug print-outs
  //ERS_DEBUG(3,"Inside an BeamModeAction: "); // nothing meaningful
}

void BeamModeAction::reset() {
  // Arbitrary placeholder...
  m_originalPS = 0;
}

// Action when going from inactive to active
bool BeamModeAction::activate() {
  ERS_LOG("BeamModeAction::activate(): ");
  m_originalPS = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);
  ERS_LOG("Making a request to un-prescale: " << m_itemName << " due to BeamMode");
  // Also: request menu update (!!)
  rulechecker().requestDraft(m_itemName, m_targetPrescale);
  return true;
}

bool BeamModeAction::deactivate() {
  // When de-activating, we try to revert to the original prescale value
  //float currentPS = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);
  //if(m_ratioPS > 0)
  //  rulechecker().requestDraft(m_itemName, (unsigned int)((currentPS/m_ratioPS)+0.5));
  //else
  //  rulechecker().requestDraft(m_itemName, (unsigned int)(0));
  ERS_LOG("BeamModeAction::deactivate(): ");
  ERS_LOG("Making a request to re-apply a prescale of : " << m_originalPS);
  rulechecker().requestDraft(m_itemName, (unsigned int) (m_originalPS+0.5));

  return true;
}

bool BeamModeAction::repeatActivate() {
  // Nothing actually happens here
  //ERS_DEBUG(3,"BeamModeAction::repeatActivate(): ");
  return true;
}

bool BeamModeAction::update() {
  // Print-out status
  print();

  // We do nothing here.  Surprise!
  return true;
}
