#include <AutoPrescaler/rulechecker/MenuDraft.h>
#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include "AutoPrescaler/prescaler/APLogger.h"

//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------

bool MenuDraft::reset(int currentL1PsKey) {
  if (m_bufferL1PsKey != currentL1PsKey) {
    m_bufferedPrescales.clear();
    m_bufferL1PsKey = currentL1PsKey;
    AP_ERS_DEBUG("Resetting the buffer");
  }
  m_currentDraft.clear(); 
  return true; 
}


MenuDraft::Draft_t MenuDraft::check() {
  if (m_currentDraft.size() == 0)
    return m_currentDraft;
  
  ERS_INFO("Checking Menu Draft");
  
  MenuDraft::Draft_t filteredDraft;

  // Compare the PS to what is currently implemented
  for (auto item : m_currentDraft) {
    std::stringstream logMsg;

    // Are we book-keeping this item?  If not, forget about it for now: misconfiguration
    if (!rulechecker().dataModel().abstraction().checkItem(item.first)) continue;

    float currentPrescale = rulechecker().dataModel().abstraction().getL1Prescale(item.first);

    if (item.second != currentPrescale) {
      filteredDraft[item.first] = item.second;
      m_bufferedPrescales[item.first] = item.second;
      logMsg << "Adding menu request for " << item.first;
    } 
    else if (m_bufferedPrescales.count(item.first) != 0 && m_bufferedPrescales.at(item.first) != item.second) {
      logMsg << "Updating the menu request for " << item.first << " buffered prescale: " << m_bufferedPrescales.at(item.first);
      filteredDraft[item.first] = item.second;
      m_bufferedPrescales[item.first] = item.second;
    }

    logMsg << " current PS is: " << rulechecker().dataModel().abstraction().getL1Prescale(item.first) 
              << " requested PS is: " << item.second;

    if (!logMsg.str().empty()) {
      ERS_INFO(logMsg.str());
    }
  }
  return filteredDraft;
}
