//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/OnlineData.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
//------------------------------------------------------------------------------
#include <ers/ers.h>
//------------------------------------------------------------------------------
#ifndef ERS_WARNING
#define ERS_WARNING(X) do { std::cout << X << std::endl; } while(false)
#endif

using namespace L1CT;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
OnlineData::OnlineData():   
            m_subscribed(false),
	    m_initialReceiver(ISServer::initialL1CT.partition()),
	    m_primaryReceiver(ISServer::ATLAS.partition()) {
  ERS_LOG("Constructing OnlineData");
  ERS_LOG("Subscribing to partition: ATLAS");
}

//------------------------------------------------------------------------------
OnlineData::~OnlineData() {
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
void OnlineData::subscribe() {

  if (m_subscribed) return;    
  // Subscribe here.. To anything in this case. 

  bool success = false;
    // We temporarily add some protection to the subscription
  while (1) {
    ERS_LOG("Attempting to subscribe to L1CT");
    try {
      // Subscribe to 'ATLAS' publications
      subscribeToPrimary("L1CT", "CTPCORE.Instantaneous.TriggerRates");
      subscribeToPrimary("L1CT", "CtpControllerLoadedL1PSKey");
      subscribeToPrimary("L1CT", "CtpControllerScheduledL1PSKey");
      subscribeToPrimary("L1CT", "AutoPrescaler.PSKHistory");
      
      m_infoSubscribed["L1CT.CtpControllerLoadedL1PSKey"] = &m_L1PSK;
      m_infoSubscribed["L1CT.CtpControllerScheduledL1PSKey"] = &m_scheduledL1PSK;
      m_infoSubscribed["L1CT.CTPCORE.Instantaneous.TriggerRates"] = &m_triggerRates;
      m_infoSubscribed["L1CT.AutoPrescaler.PSKHistory"] = &m_PSKHistory;

      ERS_LOG("Subscription successful!");
      success = true;
    } catch(...) {
      ERS_LOG("Failed to subscribe to L1CT");
      ERS_LOG("Will attempt again in 10 seconds");
      sleep(10);
    }
    if (success) break;
  }

  // Don't forget to reset the flag...
  success = false;
  while (1) {
    ERS_LOG("Attempting to subscribe to LHC: ");
    try {
      // Subscribe to 'initial' publications
      subscribeToInitial("LHC", "BeamMode");
      m_infoSubscribed["LHC.BeamMode"] = &m_beamMode;
      ERS_LOG("Subscription successful!");
      success = true;
    } catch(...) {
      ERS_LOG("Failed to subscribe to LHC");
      ERS_LOG("Will attempt again in 10 seconds");
      sleep(10);
    }
    if(success) break;
  }

    
  m_subscribed = true;
  
  // We need an initialized value for this to work
  auto & dictPrimary = ISServer::ATLAS.dictionary();
  auto & dictInitial = ISServer::initialL1CT.dictionary();
  ERS_LOG("If re-starting: we need to read in the values: ");
  try {
    dictInitial.getValue("LHC.BeamMode", m_beamMode);
    ERS_LOG("Read in the current Beam Mode: " << m_beamMode);      
    dictPrimary.getValue("L1CT.AutoPrescaler.PSKHistory", m_PSKHistory);
    ERS_LOG("Read in the current PSK History: " << m_PSKHistory);
    dictPrimary.getValue("L1CT.CtpControllerLoadedL1PSKey", m_L1PSK);
    ERS_LOG("Read in the current PSK: " << m_L1PSK);
    dictPrimary.getValue("L1CT.CtpControllerScheduledL1PSKey", m_scheduledL1PSK);
    ERS_LOG("Read in the scheduled PSK: " << m_scheduledL1PSK);
  } catch (...) {
    ERS_LOG("Values not published yet: wait for callback instead");
  }
}
//------------------------------------------------------------------------------
void OnlineData::unsubscribe() {
  AP_ERS_DEBUG("OnlineData::unsubscribe() -- enter");
    if(!m_subscribed) return;
    // Unsubscribe. Not sure if we will need it, but here we go    
    m_subscribed = false;
}
//------------------------------------------------------------------------------
void OnlineData::subscribeTo(ISInfoReceiver &receiver, const std::string &server, const ISCriteria &criteria) {
  // Do the actual subscription
  try {
    receiver.subscribe(server, criteria, &OnlineData::callback, this);
  } catch(daq::is::Exception &e) {
    const std::string partitionName = (&receiver == &m_primaryReceiver ? ISServer::ATLAS.PartitionName : ISServer::initialL1CT.PartitionName);
    ERS_WARNING("OnlineData::subscribeTo(...): "
      << "An exception occured while subscribing to "
      << "'" << partitionName << "." << server << "'"
      << " using "
      << "'" << criteria << "'"
      << "OnlineData::subscribeTo(...): Exception "
      << e.what() 
    );

    ERS_WARNING("OnlineData::subscribeTo(...): Exception .what() " << e.what());        
    throw "Temporary Exception";
  }
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
void OnlineData::update(ISCallbackInfo * isc) {

  // Prevent multiple updates at the same time
  boost::recursive_mutex::scoped_lock lock(m_mutex);
  
  // Update IS info and whatever else needs to be done. Internal flags etc.
  const std::string name  = isc->name();
  const auto type         = isc->type();
  AP_ERS_DEBUG("Update IS info: name: " << name << " type " << type);

  // Generic approach: update the map with whatever we got a callback from
  infomap_t::iterator entry = m_infoSubscribed.find(name);
  if (entry != m_infoSubscribed.end()) {
	  isc->value(*(m_infoSubscribed[name]));
    return;
  }

  return;
}
//------------------------------------------------------------------------------
