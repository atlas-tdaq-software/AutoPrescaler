//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/RuleChecker.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/PrescalerIpcCommander.h>
//------------------------------------------------------------------------------
#include "rc/RCStateInfo.h"
#include "AutoPrescaler/prescaler/APLogger.h"
//------------------------------------------------------------------------------
#include <chrono>
#include <thread>
//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------


RuleChecker::RuleChecker() {
  ERS_INFO("Constructing Rulechecker");

  // Subscribe to DataModel
  m_dataModel.add(*this);
  AP_ERS_DEBUG("Subscribed RuleChecker to DataModel");

  // ensure MenuDraft knows we exist
  m_menuDraft.setRuleChecker(*this);
  m_cachedRequest.clear();
}

void RuleChecker::addRule (CombinedRule& rulein) {
  ERS_INFO("Adding rule " << rulein.name() << " to RuleChecker");
  m_rules.push_back(rulein);
  for (auto item: rulein.needsItems()) {
    AP_ERS_DEBUG("Item needed: " << item);
    m_dataModel.abstraction().addL1Item(item);
  }
}


bool RuleChecker::checkPSKHistory() {
  // Check if the histories are aligned - if not, call a global reset
  std::vector<CtpControllerL1PSKey> publishedPSKHistory = dataModel().abstraction().online().triggerPSKHistory().items;
  
  // if published history is shorter: surely wrong
  if (publishedPSKHistory.size() < m_lastPSKHistory.size()) return false;
  
  for (unsigned int i=0; i<m_lastPSKHistory.size(); i++) {
    if (publishedPSKHistory.at(i).L1PrescaleKey != m_lastPSKHistory.at(i).L1PrescaleKey) {
	    return false;
    }
  }

  return true;
}

void RuleChecker::globalReset() {
  // Reset everything
  m_menuDraft.reset();
  m_cachedRequest.clear();
  for (unsigned int i=0; i<m_rules.size(); i++) {
    m_rules[i].reset();
  }
}


bool RuleChecker::update(ISCallbackInfo * isc) {
  // Read IS observables
  std::string logMsg = "Received IS callback - updating the rules. ";

  const std::string name  = isc->name();

  // Handle the key update - try to puclish a cached draft
  if (name == "L1CT.CtpControllerLoadedL1PSKey" 
    || name == "L1CT.CtpControllerScheduledL1PSKey"
    || name == "L1CT.AutoPrescaler.PSKHistory") {

    logMsg.append("Prescale keys were updated");
    AP_ERS_DEBUG(logMsg);
    if (m_cachedRequest.size() != 0) {
      if (checkIfKeysInSync()) {
        ERS_INFO("RuleChecker is requesting the cached prescale set");
        PrescalerIpcCommander prescaler;
        unsigned long psk = dataModel().abstraction().getL1PSK();

        bool result = prescaler.requestPrescale(psk, m_cachedRequest);
        m_cachedRequest.clear();

        return result;
      } else {
        //print keys are not in sync
        ERS_INFO("Keys are not synchronized yet: " << keysToString());
        return true;
      }
    }
    return true;
  }  

  // We only want to start putting in work if this is a rate update
  // Otherwise, we'll start doing some weird stuff...
  if (name != "L1CT.CTPCORE.Instantaneous.TriggerRates" && name !="LHC.BeamMode") {
    logMsg.append("Rates or beamMode were not updated - carry on.");
    AP_ERS_DEBUG(logMsg);
    return true;
  }      

  // Additionally, we don't want to do any work if we are not in the RUNNING state
  RCStateInfo rcStateInfo;
  auto & dict = ISServer::ATLAS.dictionary();
  dict.getValue("RunCtrl.RootController", rcStateInfo);
  if (rcStateInfo.state != "RUNNING") {
    logMsg.append("Root Controler state is not running - carry on."); 
    AP_ERS_DEBUG(logMsg);
    return true;
  }

  if (!checkPSKHistory()) {
    logMsg.append("Prescale history check failed - calling a reset!");
    globalReset();
  }  

  ERS_LOG(logMsg);
  logMsg.clear();

  // Hope this is a copy :D
  m_lastPSKHistory = dataModel().abstraction().online().triggerPSKHistory().items;
  
  // Based on the published prescale key, define whether to wipe out the draft
  // completely or just push out an update
  m_menuDraft.reset(m_lastPSKHistory.back().L1PrescaleKey);
  
  // Will need some prescale key book-keeping in RuleChecker, at the very least...

  // Update conditions and actions - do it through the rules for the moment
  AP_ERS_DEBUG("Rules size " << m_rules.size());
  for (unsigned int i=0; i < m_rules.size(); i++) {
    // Update the conditions, the actions, the state, and call the necessary action
    AP_ERS_DEBUG("Updating rule " << m_rules[i].name() );
    if (m_rules[i].update()) {
      AP_ERS_DEBUG("Update successful - calling request");
      m_rules[i].request();
    } else {
	    AP_ERS_DEBUG("Updating a rule failed");
    }
  }

  // Check the menu draft for any diff
  MenuDraft::Draft_t draftDifference = m_menuDraft.check();

  AP_ERS_DEBUG ("Menu Draft after a check. Size " << draftDifference.size());
  for (const auto &it : draftDifference) {
    AP_ERS_DEBUG("    " << it.first.c_str() << " " << it.second);
  }

  // If needed, request a PS-change
  if (draftDifference.size() > 0) {
    return requestPS(draftDifference);
  }
  
  return true;
}

bool RuleChecker::requestPS(MenuDraft::Draft_t diffRequest) {
  ERS_INFO("RuleChecker is requesting new prescale set");
  PrescalerIpcCommander prescaler;

  if (checkIfKeysInSync()) {
    unsigned long psk = dataModel().abstraction().getL1PSK();
    return prescaler.requestPrescale(psk, diffRequest);
  } else {
    ERS_INFO("Loaded, scheduled and the last AP history keys are not synchronized " << keysToString() << " - request will be cached");
    if (m_cachedRequest.size() != 0) {
      ERS_INFO("Overwriting the previous request of size " << m_cachedRequest.size());
    }
    m_cachedRequest = diffRequest;
    return true;
  }
}

bool RuleChecker::checkIfKeysInSync() {
  unsigned long psk = dataModel().abstraction().getL1PSK();
  unsigned long scheduledPsk = dataModel().abstraction().getScheduledL1PSK();
  unsigned int  pskFromHistory = dataModel().abstraction().online().triggerPSKHistory().items.back().L1PrescaleKey;

  return ((psk == pskFromHistory) && (psk == scheduledPsk));
}

std::string RuleChecker::keysToString() {
  unsigned long psk = dataModel().abstraction().getL1PSK();
  unsigned long scheduledPsk = dataModel().abstraction().getScheduledL1PSK();
  unsigned int  pskFromHistory = dataModel().abstraction().online().triggerPSKHistory().items.back().L1PrescaleKey;

  std::stringstream ss;
  ss << "CTP Loaded key " << psk << " CTP Scheduled key " << scheduledPsk << " last key in AP history " << pskFromHistory;
  return ss.str();
}
