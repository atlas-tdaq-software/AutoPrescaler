#include <AutoPrescaler/rulechecker/AvgRateCond.h>
#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>
#include <boost/date_time/posix_time/posix_time.hpp>
//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------

void AvgRateCond::reset() { 
  m_isHot = false;
  m_timeElapsed = false;
  m_rates.clear();
  m_lastValue = false;
  m_isCold = true;
}

bool AvgRateCond::truthValue() {
  // If we've been *only* over threshold for
  // the time window, change the state to HOT
  if (m_isHot && m_timeElapsed) {
    m_lastValue = true;
    return true;
  }
  
  // If we've been *only* under threshold for
  // the time window, change the state to COLD
  if (m_isCold && m_timeElapsed){
    m_lastValue = false;
    return false;
  }
  
  // If we've got a mix of hot and cold, just
  // keep the state we used to be in.
  return m_lastValue;
  
  //return (m_isHot && m_timeElapsed) ? true: false;
}

void AvgRateCond::print() {
  // Debug print-outs
  ERS_LOG("Inside an AvgRateCond: ");
  ERS_LOG("Set-up: Item Monitored: " << m_itemName);
  ERS_LOG("Set-up: maxRate / integrationTime = " << m_maxRate << " / " << m_integrationTime);

  float avg = 0;
  int entries = 0;
  float peak = 0;
  
  for (auto rate : m_rates) {
    if (rate.first > peak) {
      peak = rate.first;
    }

    avg += rate.first;
    entries++;
  }

  if (entries > 0) avg /= float(entries);

  boost::posix_time::time_duration diff = m_rates.back().second - m_rates.front().second;

  ERS_LOG("Current Status: avgRate / spikeRate / currentIntSize "                                                                                                                   
	    << avg << " / " << entries << "/"                                                                                                                                            
	    << peak << "/"                                                                                                                                                               
	    << diff.total_seconds() << " seconds");

  ERS_LOG("Item is Hot: " << m_isHot);
  ERS_LOG("Condition is Satisfied: " << truthValue());
}


bool AvgRateCond::update() {
  ERS_LOG("Entering AvgRateCond::update(): "); // Possibly remove after the test

  // Check if the monitored item is present: probably want a smarter protection
  if (!rulechecker().dataModel().abstraction().checkItem(m_itemName)) {
    ERS_LOG("Item is not in the data model abstraction: " << m_itemName);
    return false;
  }      

  // If item is disabled, we just idle
  if (!rulechecker().dataModel().abstraction().isEnabled(m_itemName)) {
    ERS_LOG("Item is disabled: " << m_itemName);
    reset();
    return false;
  }
  
  // Some ugly placeholder logic here
  float currentRate = rulechecker().dataModel().abstraction().getL1Rate(m_itemName, m_rateType);
  ERS_LOG("Current Rate: " << currentRate);

  // Book-keeping via FIFO
  std::pair<float, boost::posix_time::ptime> currentEntry(currentRate, boost::posix_time::microsec_clock::local_time());
  
  m_rates.push_back(currentEntry);

  // remove entries where time elapsed was too long ago
  while(1) {
      boost::posix_time::time_duration diff = m_rates.back().second - m_rates.front().second;
      ERS_LOG("Time delta is: " << diff.total_seconds()); // Possibly remove after the test
      if (diff.total_seconds() > m_integrationTime) {
        // First time we remove an entry, we are assured that the
        // minimum integration time has been fulfilled
        m_timeElapsed = true;
        m_rates.pop_front();
      } else {
        break;
      }
    }

  // Here we define the mechanism to determine whether we're in hot mode or not
  // For now, we ask the item to be hot over the entire integration period
  m_isHot = true;
  m_isCold = true;
  // Calculate the prescale AP: ?? not really, just checking the status
  for (auto rate : m_rates) {
    // if any of the rates in the list is smaller than the max,
    // we're definitely not hot.  If any item is higher than the max
    // then we're definitely not cold.
    if (rate.first <= m_maxRate){
      m_isHot = false;
    }
    if (rate.first > m_maxRate) {
      m_isCold = false;
    }
  }  

  print();
  
  return true;
}
