#include <AutoPrescaler/rulechecker/BeamModeCond.h>
#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>

//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------

void BeamModeCond::reset() { 
  // Do nothing
}

bool BeamModeCond::truthValue() {
  // Extremely simple logic

  // Check if the monitored item is present: probably want a smarter protection
  if (!rulechecker().dataModel().abstraction().checkItem(m_itemName)) {
    ERS_LOG("Item "<< m_itemName << " is not in the abstraction: ");
    return false;
  }      

  // If item is disabled, we just idle
  if (!rulechecker().dataModel().abstraction().isEnabled(m_itemName)) {
    ERS_LOG("Item is disabled: " << m_itemName);
    reset();
    return false;
  }
  
  std::string beamWord = rulechecker().dataModel().abstraction().getBeamMode();

  // Check if the current beam mode is in allowed beam modes
  bool goodMode = false;
  for (unsigned int i=0; i< m_beamModes.size(); i++) {
    if (beamWord == m_beamModes.at(i)) {
      goodMode = true;
    }
  }
  
  return goodMode;
}

void BeamModeCond::print() {
  // Debug print-outs
  ERS_LOG("Inside an BeamModeCond: ");
  ERS_LOG("Set-up: Item Monitored: " << m_itemName);

  for (unsigned int i=0; i< m_beamModes.size(); i++) {
    ERS_LOG("Checking for beamWords: " << m_beamModes.at(i));
  }

  ERS_LOG("Beam Mode is: " << rulechecker().dataModel().abstraction().getBeamMode());
  ERS_LOG("Condition is Satisfied: " << truthValue());
}

bool BeamModeCond::update()
{
  // Do nothing, once again.
  print();
  
  return true;
}
