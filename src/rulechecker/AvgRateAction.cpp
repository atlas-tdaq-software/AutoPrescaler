#include <AutoPrescaler/rulechecker/AvgRateAction.h>
#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>

//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------

void AvgRateAction::print() {
  // Debug print-outs
  //ERS_DEBUG(3,"Inside an AvgRateAction: ");
}

void AvgRateAction::reset() {
  m_rates.clear();
  m_requestedPS = 0;
  m_ratioPS = 1;
}

// Action when going from inactive to active
bool AvgRateAction::activate() {
  AP_ERS_DEBUG("AvgRateAction::activate(): ");
  float largestRate = 0;
  
  // Calculate the pre-scale
  for (auto rate : m_rates ) {
    if(rate.first > largestRate) {
      largestRate = rate.first;
    }
  }  
  AP_ERS_DEBUG("Largest Rate in time-window: " << largestRate);

  // Now, we look for the target rate, keeping in mind the safety factor
  float currentPS = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);
  float targetPrescale =  currentPS;
  targetPrescale *= (largestRate / (m_targetRate/m_safetyFactor));

  // Round to nearest power of 2, but can eventually use something much smarter...
  m_requestedPS = powerOf(targetPrescale);

  ERS_LOG("Making a request for item " << m_itemName << " requested ps: " << m_requestedPS);
  
  // Keeping track internally of the pre-scale before this one went hot
  if(currentPS > 0.)
    m_ratioPS = float(m_requestedPS)/float(currentPS);
  else
    m_ratioPS = 0;

  // Also: request menu update (!!)
  rulechecker().requestDraft(m_itemName, (unsigned int)(m_requestedPS));
  m_lastHot = boost::posix_time::microsec_clock::local_time();
  //= now();
  
  return true;
}

bool AvgRateAction::deactivate() {
  // When de-activating, we basically try to revert the last change we did
  float currentPS = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);
  if(m_ratioPS > 0)
    rulechecker().requestDraft(m_itemName, (unsigned int)((currentPS/m_ratioPS)+0.5));
  else
    rulechecker().requestDraft(m_itemName, (unsigned int)(0));

  return true;
}

bool AvgRateAction::repeatActivate() {
  AP_ERS_DEBUG("AvgRateAction::repeatActivate(): ");
  // Rare case: if hot item went even hotter...
  float largestRate = rulechecker().dataModel().abstraction().getL1Rate(m_itemName, m_rateType);
  float currentPS = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);
  
  // Use the requested PS
  if (m_requestedPS > 0) {
    largestRate /= m_requestedPS;
  }
  else if (currentPS > 0) {
    largestRate /= currentPS;
  }
  
  boost::posix_time::time_duration diff = boost::posix_time::microsec_clock::local_time() - m_lastHot;
  float timeElapsed = diff.total_microseconds()/1.e6;
  if(timeElapsed > m_integrationTime && largestRate  > m_targetRate)
    {
      ERS_LOG("Rate for " << m_itemName << " went even hotter " << largestRate);
      
      // Now, we look for the target rate, keeping in mind the safety factor

      float targetPrescale =  largestRate / (m_targetRate/m_safetyFactor);

      // Round to nearest power of 2, but can eventually use something much smarter...
      // Since this should be using TAP - the new prescale is calculated on top of
      // the last prescale
      float lastPrescale = float(m_requestedPS);
      m_requestedPS = powerOf(lastPrescale*targetPrescale);
      ERS_LOG("Making a request for: " << m_requestedPS);

      // Keeping track internally of the pre-scale before this one went hot
      //      float currentPS = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);

      // Multiply the ratio
      if (lastPrescale > 0.) {
        m_ratioPS *= float(m_requestedPS)/float(lastPrescale);
      } else {
        m_ratioPS = 0;
      }

      // Re-applied pre-scale
      m_lastHot = boost::posix_time::microsec_clock::local_time();
      //now();
    }
  
  // In both cases, make a request
  // If not enough time has passed, or the rate is stable, no action is taken
  // If the time for rate integration has passed, we allow a new request if
  // the item went even hotter...
  rulechecker().requestDraft(m_itemName, (unsigned int)(m_requestedPS));
  
  return true;
}

bool AvgRateAction::update() {
  // Print-out status
  print();
  
  // Check if the monitored item is present: probably want a smarter protection
  if (!rulechecker().dataModel().abstraction().checkItem(m_itemName)) {
    return false;
  }

  // Some ugly placeholder logic here
  float currentRate = rulechecker().dataModel().abstraction().getL1Rate(m_itemName, m_rateType);
  float currentPS   = rulechecker().dataModel().abstraction().getL1Prescale(m_itemName);
  ERS_LOG("Current Rate: " << currentRate);
  if (currentPS > 0) {
    currentRate /= currentPS;
  }
    
  ERS_LOG("Current Rate after PS: " << currentRate);
  
  // Book-keeping via FIFO
  std::pair<float, boost::posix_time::ptime> currentEntry(currentRate, boost::posix_time::microsec_clock::local_time());
  //now());
  
  m_rates.push_back(currentEntry);
  
  // remove entries where time elapsed was too long ago
  boost::posix_time::time_duration diff;
  while(1) {
    diff = m_rates.back().second - m_rates.front().second;

    if (diff.total_seconds() > m_integrationTime) {
      m_rates.pop_front();
    } else {
      break;
    }
  }

  return true;
}

int AvgRateAction::powerOf(float targetPrescale) {
  int prescale = 1;
  while (prescale < targetPrescale) {
    prescale*=m_roundingFactor;
  }

  return prescale;
}
