//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/DataAbstraction.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
DataAbstraction::DataAbstraction(OnlineData &onl):
  m_online(onl) {

  // Subscribe to notifications of online data.
  // When online gets updated, we can update self
  AP_ERS_DEBUG("Constructing DataAbstraction");
  AP_ERS_DEBUG("Added DataAbstraction to the Online callbacks");
  onl.add(*this);
}
//------------------------------------------------------------------------------
DataAbstraction::~DataAbstraction() {}
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
void DataAbstraction::update(ISCallbackInfo * /*isc*/) {
  // Update whatever you need to update

  // Need to eventually add protection for different callbacks
  // Shouldn't we wipe the map?
  
  // Actually, if we clean it, we lose the 'first prescale'
  //  m_L1ItemsDetails.clear();

  // Determine if we will store the original prescale..
  bool willRead = false;
    
  // Protection if we never read in any values...
  if (online().triggerPSKHistory().items.size() > 0) {
    if (online().triggerPSKHistory().items.front().L1PrescaleKey == online().triggerL1PSK().L1PrescaleKey){
      willRead = true;
    }
  }
    
  AP_ERS_DEBUG("The size of online trigger rates " << online().triggerRates().items.size());

  for(auto item : online().triggerRates().items) {
    if(m_L1ItemsMonitored.count(item.name) > 0) {
	    AP_ERS_DEBUG("Keeping track of: " << item.name);

	    // Extra handling for incorrect publication
	    float ratio = item.prescale;
      // HERE IS THE BUG the prescale calculated by CTP is one less then one from the RuleBook

	    //if(item.tap > 0)
	    //  ratio = item.tbp/item.tap;
	    AP_ERS_DEBUG("Enabled/Prescale/CTPPrescale/TBP/TAP/TAV/TBPdivTAP: "                                                                                                                     
		    << item.enabled << " / "                                                                                                                                             
		    << item.prescale << " / "                                                                                                                                            
		    << ratio << " / "                                                                                                                                                    
		    << item.tbp << " / "                                                                                                                                                 
		    << item.tap << " / "                                                                                                                                                 
		    << item.tav << " / "
        << item.tbp/item.tap);

      // Keep track of the prescale associated with the original key...
      // As a back-up, assume the original item is unprescaled - safest thing to assume
      // This should never happen unless some initialization went wrong...
      float oldPrescale = 1;
	    if(willRead)
	      oldPrescale = ratio;
	      //oldPrescale = item.prescale;
	    else {
	      if(m_L1ItemsDetails.count(item.name) > 0) {
		      oldPrescale = m_L1ItemsDetails.at(item.name).originalprescale;
        }
	    }
	  
      //m_L1ItemsDetails[item.name] = L1Item(item.prescale, oldPrescale, item.tbp, item.tap, item.tav);
      m_L1ItemsDetails[item.name] = L1Item(item.enabled, ratio, oldPrescale, item.tbp, item.tap, item.tav);
    }
  }
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
float DataAbstraction::getL1Rate(std::string trigName, DataAbstraction::L1Item::Type rateType) const {
  switch(rateType) {
    case L1Item::TBP:
      return m_L1ItemsDetails.at(trigName).rateTBP;
	    break;
    case L1Item::TAP:
      return m_L1ItemsDetails.at(trigName).rateTAP;
	    break;
    case L1Item::TAV:
      return m_L1ItemsDetails.at(trigName).rateTAV;
	    break;
    case L1Item::TOP:
      if (m_L1ItemsDetails.at(trigName).originalprescale > 0.){
        return m_L1ItemsDetails.at(trigName).rateTBP / m_L1ItemsDetails.at(trigName).originalprescale;
      }
      else return 0;
   default:
      break;
  }
  
  // Some semi-bogus default case here, should use an exception instead
  return m_L1ItemsDetails.at(trigName).rateTBP;
}
//------------------------------------------------------------------------------
