#include <AutoPrescaler/rulechecker/PrescalerRule.h>
#include <iostream>
//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
bool PrescalerRule::callAction()
{
  
  if(m_currentState == PrescalerRule::Inactive)
    {
      if(m_lastState == PrescalerRule::Active)
	m_action->deactivate();

      if(m_lastState == PrescalerRule::Inactive)
	m_action->repeatDeactivate();
    }
  
  if(m_currentState == PrescalerRule::Active)
    {
      if(m_lastState == PrescalerRule::Inactive)
	m_action->activate();
      
      if(m_lastState == PrescalerRule::Active)
	m_action->repeatActivate();
    }
  
  return true;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
std::vector<std::string> PrescalerRule::needsItems()
{
  std::cout << "PrescalerRule::needsItems" << std::endl;
  std::vector<std::string> condItems;
  condItems = m_condition->needsItems();
  
  std::vector<std::string> actItems;
  actItems = m_action->needsItems();
  
  condItems.insert(condItems.end(), actItems.begin(), actItems.end());
  
  std::cout << "PrescalerRule::needsItems size: " << condItems.size() << std::endl;
  return condItems;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void PrescalerRule::update()
{
  m_lastState = m_currentState;
  m_condition->truthValue() ? m_currentState = PrescalerRule::Active : m_currentState = PrescalerRule::Inactive;
}
//------------------------------------------------------------------------------
