//======================================================================== about
//------------------------------------------------------------------------------


//===================================================================== #include
#include <iostream>
//------------------------------------------------------------------------------
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/partition.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/prescaler/Prescaler.h>
#include <AutoPrescaler/ipc/PrescalerIpc.h>
//------------------------------------------------------------------------------
//#include <ers/ers.h>

//==================================================================== namespace
using namespace L1CT;
//------------------------------------------------------------------------------


//========================================================== (pseudo) int main()
int main(int argc, char ** argv) {

    // Define command line arguments
    CmdArgStr   partitionName('p',  "partition", "partition",  "Partition name. Default is ATLAS.");
    CmdArgStr   applicationName('n',  "name", "name",  "Application name. To be used later.");
    CmdArgStr   segmentName('s',  "segment", "segment",  "Segment Name. Unused atm.");

    CmdArgInt   verbose('v',        "verbose", "verbose", "Level of verboseness. VERBOSE = 5, DEBUG = 4, INFO = 3, WARNING = 2, ERROR = 1, NONE = 0. Default is 3");
    

    partitionName   = "ATLAS";
    verbose         = 4;
    applicationName = "";
    segmentName     = "";

    // Register arguments
    CmdLine cmd(argv[0],
                &partitionName,
		&applicationName,
		&segmentName,
                &verbose,
                NULL);

    CmdArgvIter argit(argc-1, argv+1);
    cmd.parse(argit);

    const std::string partitionNameInp = static_cast<std::string>(partitionName);
    if(partitionNameInp != ISServer::ATLAS.PartitionName) {
        ISServer::ATLAS.redefine(partitionNameInp, ISServer::ATLAS.ServerName);
    }

    // Start up the machinery
    IPCCore::init(argc, argv);

    ERS_INFO("Reading in parameters");
	    ERS_INFO(partitionName);
	    ERS_INFO(applicationName);
	    ERS_INFO(segmentName);

    Prescaler prescaler;
    
    const std::string ipcName = getenv("TDAQ_APPLICATION_NAME") == nullptr ? "CtpAutoPrescaler" : getenv("TDAQ_APPLICATION_NAME");
    PrescalerIpc ipc(ipcName, prescaler);
    ERS_INFO("Setting log-level to " << int(verbose));
    ERS_INFO(" Sleeping...");
    while(1){
      sleep(8);
      prescaler.processRequests();
    }

    return 0;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------

