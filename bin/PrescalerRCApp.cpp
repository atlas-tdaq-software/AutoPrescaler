#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/ItemCtrl/ControllableDispatcher.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/FSM/FSMStates.h"

#include <ers/ers.h>
#include <dal/Segment.h>

#include <boost/program_options.hpp>

#include <iostream>
#include <unistd.h>
#include <string>
#include <vector>
#include <random>
#include <thread>
#include <chrono>

#include "AutoPrescaler/prescaler/PrescalerWrapper.h"

namespace {
  ERS_DECLARE_ISSUE(rc, RandomFailure, ERS_EMPTY, ERS_EMPTY)
}

namespace po = boost::program_options;

int main(int argc, char** argv) {
  po::options_description desc("Simple run-control application sleeping for a random amount of time during state transitions");
  
  try {
    // Parser for this application's specific options: note that the "help" is not managed here

    //desc.add_options()("sleep,S", po::value<unsigned int>(&sleepFor)->default_value(sleepFor), "Maximum sleep time (in seconds)")
    //("failure,F", "Flag for activating random failures");

    std::string verbose = "INFO";
    bool isDebug = false;

    ERS_LOG(" *** Defining Parsing Params ***");
    
    desc.add_options() 
      ("loglevel,l", po::value<std::string>(&verbose)->default_value(verbose),"The verbosity level: INFO or DEBUG")
      ("debug,d", po::value<bool>(&isDebug)->default_value(false),"The debug log is turned on");
    
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
    po::notify(vm);
    
    // if(vm.count("failure")) {
    // }

    if (vm.count("loglevel") && (!(verbose == "INFO" || verbose == "DEBUG"))) {
      throw po::validation_error(po::validation_error::invalid_option_value, "Invalid loglevel option - available values: INFO, DEBUG");
    }
    
    // Parser for the command line options required by the Run Control framework
    // Note that the "help" functionality is enabled
    daq::rc::CmdLineParser cmdParser(argc, argv, true);
    
    // Here the ItemCtrl is created: the same Controllable will be executed twice in a parallel way
    // Several different Controllable instances may be passed
    // The ParallelDispatcher is just used as an example

    ERS_LOG(" *** Defining Wrapper ***");
    
    PrescalerWrapper* myWrapper = new PrescalerWrapper();
    myWrapper->passVerbose( (isDebug || verbose == "DEBUG") ? 2 : 3 ); // DEBUG = 2, INFO = 3
    myWrapper->passPartition(cmdParser.partitionName());
    
    //daq::rc::ItemCtrl itemCtrl(cmdParser, std::shared_ptr<daq::rc::Controllable>( new L1CTCool::Watchdog() ));
    daq::rc::ItemCtrl itemCtrl(cmdParser, std::shared_ptr<daq::rc::Controllable>( myWrapper ));
    
    // The ItemCtrl is initialized
    ERS_LOG(" *** Initializing ***");
    itemCtrl.init();
    
    // Run the ItemCtrl: the application is ready to accept and execute commands
    // The PMG sync is done and the ProcessManagement system will report the process as up
    // This blocks
    ERS_LOG(" *** Running ***");
    itemCtrl.run();
  }
  catch(daq::rc::CmdLineHelp& ex) {
    // Show the help message: note that both messages from the CmdLineParser and the specific parser are reported
    std::cout << desc << std::endl;
    std::cout << ex.message() << std::endl;
  }
  catch(ers::Issue& ex) {
    // Any exception thrown during the ItemCtrl construction or initialization is a fatal error
    ers::fatal(ex);
    return EXIT_FAILURE;
  }
  catch(boost::program_options::error& ex) {
    // This may come from the library used to parse this application's specific options
    ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
    return EXIT_FAILURE;
  }
  
  return EXIT_SUCCESS;
}

