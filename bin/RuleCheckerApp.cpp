//======================================================================== about
//------------------------------------------------------------------------------


//===================================================================== #include
#include <iostream>
//------------------------------------------------------------------------------
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/partition.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include <AutoPrescaler/rulechecker/PrescalerRule.h>
#include <AutoPrescaler/rulechecker/AvgRateCond.h>
#include <AutoPrescaler/rulechecker/AvgRateAction.h>
#include <AutoPrescaler/rulechecker/BeamModeCond.h>
#include <AutoPrescaler/rulechecker/BeamModeAction.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>
//------------------------------------------------------------------------------


//==================================================================== namespace
using namespace L1CT;
using namespace std;
//------------------------------------------------------------------------------


//========================================================== (pseudo) int main()
int main(int argc, char ** argv) {

    // Define command line arguments
    CmdArgStr   partitionName('p', "partition", "partition",  "Partition name. Default is ATLAS.");
    CmdArgStr   ratesItems('i',  "items", "items",  "List of ; delimited items");
    CmdArgStr   ratesMax('r',  "rates", "rates",  "List of ; delimited rates");
    CmdArgStr   beamItems('b',  "beam", "beam",  "List of ; delimited beam items");
    CmdArgInt   verbose('v',  "verbose", "verbose", "Level of verboseness. VERBOSE = 5, DEBUG = 4, INFO = 3, WARNING = 2, ERROR = 1, NONE = 0. Default is 3");
    CmdArgStr   applicationName('n',  "name", "name",  "Application name. To be used later.");
    CmdArgStr   segmentName('s',  "segment", "segment",  "Segment Name. Unused atm.");

    partitionName   = "ATLAS";
    verbose         = 4;
    ratesItems      = "L1_RD0_FILLED;L1_RD1_FILLED;L1_RD2_FILLED;L1_RD3_FILLED";
    ratesMax        = "500;500;500;500";
    beamItems       = "L1_J12";

    // Register arguments
    CmdLine cmd(argv[0],
                &partitionName,
                &verbose,
		&ratesItems,
		&beamItems,
		&ratesMax,
		&applicationName,
		&segmentName,
                NULL);
    
    CmdArgvIter argit(argc-1, argv+1);
    cmd.parse(argit);

    std::string myItems = static_cast<std::string>(ratesItems);
    std::string myRates = static_cast<std::string>(ratesMax);
    std::string myBeamItems = static_cast<std::string>(beamItems);

    std::replace( myItems.begin(), myItems.end(), ';', ' ');
    std::replace( myBeamItems.begin(), myBeamItems.end(), ';', ' ');
    std::replace( myRates.begin(), myRates.end(), ';', ' ');
    istringstream iss(myItems);
    vector<string> tokens{istream_iterator<string>{iss},
	istream_iterator<string>{}};
    istringstream iss2(myRates);
    vector<string> tokens2{istream_iterator<string>{iss2},
	istream_iterator<string>{}};
    istringstream iss3(myBeamItems);
    vector<string> tokens3{istream_iterator<string>{iss3},
	istream_iterator<string>{}};

    std::vector<std::pair<std::string, float> > myConfiguration;
    std::vector<std::string> myBeamConfig;

    std::cout << "Parsing configuration.  Read in items: "<< std::endl;
    for(unsigned int i=0; i<tokens.size(); i++)
      {
	myConfiguration.push_back(std::pair<std::string, float>(tokens.at(i), stof(tokens2.at(i).c_str())));
	std::cout << tokens.at(i) << " " << stof(tokens2.at(i).c_str()) << std::endl;
	std::cout << myConfiguration.at(i).first << ";" << myConfiguration.at(i).second << std::endl;
      }

    for(unsigned int i=0; i<tokens3.size(); i++)
      {
	myBeamConfig.push_back(tokens3.at(i));
      }

    
    const std::string partitionNameInp = static_cast<std::string>(partitionName);
    if(partitionNameInp != ISServer::ATLAS.PartitionName) {
        ISServer::ATLAS.redefine(partitionNameInp, ISServer::ATLAS.ServerName);
    }

    ISServer::initialL1CT.redefine("initial", ISServer::ATLAS.ServerName);
    
    // Start up the machinery
    IPCCore::init(argc, argv);

    RuleChecker ruleChecker;
    
    ERS_LOG("Setting log-level to " << int(verbose));

    // Define rules - hard-code the initialization
    ERS_LOG(" Ready to initialize: ");    
    //PrescalerCondition* myCond = new AvgRateCond("L1_EM3", 500., 5);
    //PrescalerAction* myAct = new AvgRateAction("L1_EM3", 200., 3);

    
    /*
      myConfiguration.push_back(std::pair<std::string, float>("L1_RD0_FILLED", 1500.));
    myConfiguration.push_back(std::pair<std::string, float>("L1_RD1_FILLED", 3000.));
    myConfiguration.push_back(std::pair<std::string, float>("L1_RD2_FILLED", 6000.));
    myConfiguration.push_back(std::pair<std::string, float>("L1_RD3_FILLED", 12000.));
    myConfiguration.push_back(std::pair<std::string, float>("L1_EM3", 500.));
    */
    
    // Allocate Rules automatically
    std::vector<PrescalerCondition*> myConditions;
    std::vector<PrescalerAction*> myActions;
    std::vector<PrescalerRule> myRules;
    for( auto item: myConfiguration )
      {
	// Default mass initialization
	// Note: We use a time-window of 40 seconds
	// Safety Factor of 3 (ie: prescale to a rate 3 times lower than the maxRate)
	// Prescales in powers of 2.

	// Parameters: Trigger, maxRate, Integration window (in secs)
	// Important: ## Condition should use TOP -> "Trigger with Original Prescale"	
	myConditions.push_back(new AvgRateCond(item.first, item.second, 40., DataAbstraction::L1Item::Type::TOP));
	
	// Parameters: Trigger, maxRate, safetyFactor, roundingPowers, Integration window (in secs)
	// Important: ## Action should use TAP -> "Trigger after Prescale", since this 
	//               is the rates we want to use to calculate current prescales
	myActions.push_back(new AvgRateAction(item.first, item.second, 3, 2, 40., DataAbstraction::L1Item::Type::TBP));
	
	// Create the rule
	myRules.push_back(PrescalerRule(myConditions.back(), myActions.back()));
	myActions.back()->setRuleChecker(ruleChecker);
	myConditions.back()->setRuleChecker(ruleChecker);
	//ruleChecker.addRule(myRules.back());
      }
    
    std::vector<std::string> beamModes;
    beamModes.push_back("FLAT TOP");
    beamModes.push_back("SQUEEZE");

    // Create all BeamItem configurations
    for( auto item: myBeamConfig )
      {
	ERS_LOG(" Creating condition: Beam Mode " << item);

	myConditions.push_back(new BeamModeCond(item, beamModes));

	// Parameters: Trigger, maxRate, safetyFactor, roundingPowers, Integration window (in secs)
	// Important: ## Action should use TAP -> "Trigger after Prescale", since this
	//               is the rates we want to use to calculate current prescales
        ERS_LOG(" Creating action: Beam Mode " << item);

	myActions.push_back(new BeamModeAction(item, 1));

	// Create the rule
	
	ERS_LOG(" Creating rule: Beam Mode " << item );
	myRules.push_back( PrescalerRule(myConditions.back(), myActions.back()));

	myActions.back()->setRuleChecker(ruleChecker);
	myConditions.back()->setRuleChecker(ruleChecker);
	ERS_LOG(" Adding rule: Beam Mode " << item);
	//ruleChecker.addRule(myRules.back());
      }
    

    //PrescalerCondition* myCond = new AvgRateCond("L1_3J15", 500., 40, DataAbstraction::L1Item::Type::TOP);
    // Parameters: Trigger, maxRate, safetyFactor, roundingPowers, Integration window (in secs)
    // Important: ## Action should use TAP -> "Trigger after Prescale", since this 
    //               is the rates we want to use to calculate current prescales
    //PrescalerAction* myAct = new AvgRateAction("L1_3J15", 500., 3, 2, 40, DataAbstraction::L1Item::Type::TAP);
    //PrescalerRule myRule(myCond, myAct);

    //PrescalerCondition* myCond2 = new AvgRateCond("L1_RD0_FILLED", 1500., 40, DataAbstraction::L1Item::Type::TOP);
    // Parameters: Trigger, maxRate, safetyFactor, roundingPowers, Integration window (in secs)
    // Important: ## Action should use TAP -> "Trigger after Prescale", since this 
    //               is the rates we want to use to calculate current prescales
    //PrescalerAction* myAct2 = new AvgRateAction("L1_RD0_FILLED", 1500., 3, 2, 40, DataAbstraction::L1Item::Type::TAP);
    //PrescalerRule myRule2(myCond2, myAct2);

    // Set up the interdependency
    //myCond->setRuleChecker(ruleChecker);
    //myAct->setRuleChecker(ruleChecker);

    //myCond2->setRuleChecker(ruleChecker);
    //myAct2->setRuleChecker(ruleChecker);

    // Add rule to RuleChecker
    ERS_LOG(" Adding Rule: ");    
    //ruleChecker.addRule(myRule);
    //ruleChecker.addRule(myRule2);

    // Normal operation
    ERS_LOG(" Sleeping... ");
    while(1){
      sleep(1);
    }

    // For now, the rulechecker only needs to sleep (!!)
    return 0;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------

