package L1CTIPC;

//**************************************
//
// PrescaleWriterServant: Listens to CORBA requests for new prescale keys
// Inherits from the IDL-generated class
//
//**************************************

public class PrescaleWriterServant extends ipc.NamedObject<L1CTIPC.PrescaleWriterInterface> implements L1CTIPC.PrescaleWriterInterfaceOperations {
    private final PrescaleWriterOperations ap;

    // Wrapper for the overriden classes
    public static interface PrescaleWriterOperations {
	// Prescale request
	// Will be overriden in the main class
	public L1CTIPC.PrescaleReturn requestPrescale(final L1CTIPC.PrescaleRequest request) throws Exception;

    }

    // Constructor
    public PrescaleWriterServant(final String partitionName, final String name, final PrescaleWriterOperations ap)
	throws ipc.InvalidPartitionException
    {
	this(new ipc.Partition(partitionName), name, ap);
    }

    // Allocator, publishes to IPC
    public PrescaleWriterServant(final ipc.Partition ipcPartition, final String name, final PrescaleWriterOperations ap)
	throws ipc.InvalidPartitionException
    {
	super(ipcPartition, name);
	this.ap = ap;

	this.publish();
    }

    // Wrapper to override the IDL defined functions
    @Override
    public L1CTIPC.PrescaleReturn requestPrescale(final L1CTIPC.PrescaleRequest request) throws L1CTIPC.InvalidName,L1CTIPC.ObsoletePsk {
	try {
	    return this.ap.requestPrescale(request);
	}
	catch(final Exception ex) {
	    throw new InvalidName("Dummy Error");
	}

	//return new L1CTIPC.PrescaleReturn();
    }

}
