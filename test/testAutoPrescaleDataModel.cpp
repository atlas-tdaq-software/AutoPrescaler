//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
//------------------------------------------------------------------------------
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/partition.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/DataModel.h>
#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include <AutoPrescaler/rulechecker/AvgRateCond.h>
#include <AutoPrescaler/rulechecker/AvgRateAction.h>
#include <AutoPrescaler/rulechecker/PrescalerAction.h>
#include <AutoPrescaler/rulechecker/PrescalerRule.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
using namespace L1CT;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
int main(int argc, char **argv) {

    CmdArgStr   partition('p', "partition", "partition", "Name of partition. Default ATLAS");
    CmdArgStr   server('n', "server", "server", "Name of IS sever. Default L1CT.");
    CmdArgInt   verbose('v', "verbose", "verbose", "Level of verboseness. VERBOSE = 5, DEBUG = 4, INFO = 3, WARNING = 2, ERROR = 1, NONE = 0. Default is 5");
    
    partition   =   ISServer::ATLAS.PartitionName.c_str();
    server      =   ISServer::ATLAS.ServerName.c_str();
    verbose     =   5;

    CmdLine cmd(argv[0],
                &partition,
                &server,
                &verbose,
                NULL);

    CmdArgvIter argit(argc-1, argv+1);
    cmd.parse(argit);
    
    
    const std::string p = static_cast<std::string>(partition);
    const std::string n = static_cast<std::string>(server);
    if(p != ISServer::ATLAS.PartitionName || n != ISServer::ATLAS.ServerName) {
        ISServer::ATLAS.redefine(p, n);
    }

    ERS_INFO("Configuration of the day:");
    ERS_INFO(" - partition: " << p);
    ERS_INFO(" - IS server: " << n);
    
    // Start up the machinery
    IPCCore::init(argc, argv);

    //DataModel model;

    // Very ugly set-up for test

    // Prepare Prescaler
    Prescaler myPrescaler;
    
    // Define rules
    PrescalerCondition* myCond = new AvgRateCond("L1_EM3", 500., 5);
    PrescalerAction* myAct = new AvgRateAction("L1_EM3", 200., 3);
    PrescalerRule myRule(myCond, myAct);
    
    // Define RuleChecker
    ERS_INFO(" Ready to initialize: ");
    RuleChecker myChecker;
    
    // Set up the interdependency
    myCond->setRuleChecker(myChecker);
    myAct->setRuleChecker(myChecker);
    
    // Add rule to RuleChecker
    ERS_INFO(" Adding Rule: ");
    myChecker.addRule(myRule);
    
    // Attach Prescaler
    ERS_INFO(" Adding Rule: ");
    myChecker.attachPrescaler(&myPrescaler);

    // Normal operation
    ERS_INFO(" Sleeping... ");    
while(1){
      sleep(5);
      myPrescaler.processRequests();
    }
    return 1;
}

