#ifndef AutoPrescaler_ipc_PrescalerIpcCommander_h
#define AutoPrescaler_ipc_PrescalerIpcCommander_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
#include <AutoPrescaler/AutoPrescaler.hh>
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/MenuDraft.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class PrescalerIpcCommander {

public:
    PrescalerIpcCommander(IPCPartition &partition, const std::string &name)
        : m_partition(partition), m_name(name) {}
    PrescalerIpcCommander(const std::string &name) : PrescalerIpcCommander(ISServer::ATLAS.partition(), name) {}
    PrescalerIpcCommander() : PrescalerIpcCommander(ISServer::ATLAS.partition(), prescalerNameOKS()) {}

    virtual ~PrescalerIpcCommander() {}

public:
    bool requestPrescale(const unsigned long psk, const MenuDraft::Draft_t &draft);


protected:
    inline ::L1CTIPC::PrescalerInterface_var prescaler() {
        return m_partition.lookup < ::L1CTIPC::PrescalerInterface >(m_name); 
    }

private:
    const std::string prescalerNameOKS() { return "CtpAutoPrescaler"; }

    IPCPartition &      m_partition;
    const std::string   m_name;

};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif
