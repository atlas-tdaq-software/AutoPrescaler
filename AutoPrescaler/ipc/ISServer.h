#ifndef AutoPrescaler_ipc_ISServer_h
#define AutoPrescaler_ipc_ISServer_h
//------------------------------------------------------------------------------
#include <iostream>
#include <string>
//------------------------------------------------------------------------------
#include <ipc/partition.h>
#include <is/infodictionary.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
namespace L1CT {
  //------------------------------------------------------------------------------


  //------------------------------------------------------------------------------
  struct ISServer {
    virtual ~ISServer() { reset(); }

    ISServer(const std::string& p, const std::string &s)
    : PartitionName(m_partitionName), 
      ServerName(m_serverName),
      m_partitionName(p),
      m_serverName(s),
      m_partition(nullptr),
      m_dict(nullptr) {
    }

    void reset() {
      delete m_dict; m_dict = nullptr;
      delete m_partition; m_partition = nullptr;
    }

    const std::string & PartitionName;
    const std::string & ServerName;

    IPCPartition &partition() { 
      if(m_partition == nullptr) m_partition = new IPCPartition(PartitionName);
      return *m_partition;
    }

    ISInfoDictionary &dictionary() {
      if(m_dict == nullptr) m_dict = new ISInfoDictionary(partition());
      return *m_dict;
    }
  
    void redefine(const std::string &partitionName, const std::string &serverName) {

      std::cerr << "WARNING: Changing ISServer definition from " 
                  << "'"<< m_partitionName << "." << m_serverName << "'"
                  << " to "
                  << "'"<< partitionName << "." << serverName << "'"
		<< std::endl;

      reset();

      m_partitionName     =   partitionName;
      m_serverName        =   serverName;
    }


    static ISServer ATLAS;
    static ISServer initialL1CT;

    static ISServer PerBunchHistograms;

  private:
    std::string         m_partitionName;
    std::string         m_serverName;

    IPCPartition *      m_partition;
    ISInfoDictionary *  m_dict;
  };
  //------------------------------------------------------------------------------


  //------------------------------------------------------------------------------
} // Namespace
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif
