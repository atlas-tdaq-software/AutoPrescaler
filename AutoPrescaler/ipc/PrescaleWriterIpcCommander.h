#ifndef AutoPrescaler_ipc_PrescaleWriterIpcCommander_h
#define AutoPrescaler_ipc_PrescaleWriterIpcCommander_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
//#include <TTCInfo/PrescaleWriter.hh>
#include <PrescaleWriter/PrescaleWriter.hh>
//------------------------------------------------------------------------------
#include <AutoPrescaler/prescaler/PrescaleBuffer.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class PrescaleWriterIpcCommander {

public:
    PrescaleWriterIpcCommander(IPCPartition &partition, const std::string &name)
        : m_partition(partition), m_name(name) {}
    PrescaleWriterIpcCommander(const std::string &name) : PrescaleWriterIpcCommander(ISServer::ATLAS.partition(), name) {}
    PrescaleWriterIpcCommander() : PrescaleWriterIpcCommander(ISServer::ATLAS.partition(), prescaleWriterNameOKS()) {}

    virtual ~PrescaleWriterIpcCommander() {}

public:
    int createPrescale(const unsigned long psk, const PrescaleBuffer &prescales);


protected:
    inline ::L1CTIPC::PrescaleWriterInterface_var prescaler() {
        return m_partition.lookup < ::L1CTIPC::PrescaleWriterInterface >(m_name); 
    }
    
 private:
    const std::string prescaleWriterNameOKS() { return "AutoPrescaleJavaWriter"; }
    
    IPCPartition &      m_partition;
    const std::string   m_name;

};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif
