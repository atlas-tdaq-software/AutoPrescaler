#ifndef AutoPrescaler_ipc_PrescalerIpc_h 
#define AutoPrescaler_ipc_PrescalerIpc_h 
//------------------------------------------------------------------------------
#include <AutoPrescaler/ipc/ISServer.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/AutoPrescaler.hh>
//------------------------------------------------------------------------------
#include <AutoPrescaler/prescaler/Prescaler.h>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
class PrescalerIpc
  : public IPCNamedObject<POA_L1CTIPC::PrescalerInterface, ::ipc::multi_thread>
{

public:
    PrescalerIpc(const std::string &name, L1CT::Prescaler &prescaler)
        : IPCNamedObject<POA_L1CTIPC::PrescalerInterface, ::ipc::multi_thread>(ISServer::ATLAS.partition(), name),
          m_prescaler(prescaler) {

        // Taken from (some) template 
        try {
            publish();
        } catch(daq::ipc::Exception& ex) {
            ers::error(ex);
        }
    }

    virtual ~PrescalerIpc() {}

    virtual void shutdown() override {
        // Taken from (some) template 
        try {
            withdraw();
            _destroy(true);
        } catch(daq::ipc::Exception& e) {
        }
    }

public:
    virtual ::CORBA::Boolean requestPrescale(const L1CTIPC::PrescaleRequest& request) override;


protected:
    inline L1CT::Prescaler &prescaler() { return m_prescaler; }
    inline const L1CT::Prescaler &prescaler() const { return m_prescaler; }


private:
    L1CT::Prescaler & m_prescaler;

};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
#endif

