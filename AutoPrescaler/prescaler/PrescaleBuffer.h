#ifndef AutoPrescaler_prescaler_PrescaleBuffer_h
#define AutoPrescaler_prescaler_PrescaleBuffer_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <deque>
#include <string>
#include <boost/date_time/posix_time/posix_time.hpp>

//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------

  class PrescaleBuffer {

  public:
    struct PrescaleEntry {
      // Potentially define functions useful for manipulations here
      boost::posix_time::ptime timeStamp;
      std::string triggerName;
      float prescale;
      unsigned int prescaleKey;
    };
    
    virtual ~PrescaleBuffer() {}
    PrescaleBuffer() {}

    // Iterators for public access
    // Map-like interface
    typedef std::deque<PrescaleEntry>::iterator       iterator;
    typedef std::deque<PrescaleEntry>::const_iterator const_iterator;

    // Iterators and Accessors over PrescaleBuffer
    iterator begin() { return m_contents.begin(); }
    const_iterator begin() const { return m_contents.begin(); }

    iterator end() { return m_contents.end(); }
    const_iterator end() const { return m_contents.end(); }

    size_t size() const { return m_contents.size(); }

    const PrescaleEntry& back() const { return m_contents.back(); }
    void push_front(const PrescaleEntry& myEntry) { m_contents.push_front(myEntry); }
    void pop_back() { m_contents.pop_back(); }
    void rejoinBuffers(PrescaleBuffer& myBuffer);


    PrescaleEntry &operator [] (const unsigned int &n) { return m_contents[n]; }
    const PrescaleEntry &operator [] (const unsigned int &n) const { return m_contents.at(n); }

    // Hopefully this works ^^
    inline PrescaleBuffer& operator= (const PrescaleBuffer rhs) { m_contents = rhs.m_contents; return *this; }

    // Clear Entire Buffer
    inline void clearBuffer()
    { m_contents.clear(); }
    
    // Add a new entry to the buffer
    inline void addEntry(PrescaleEntry myEntry)
    { m_contents.push_back(myEntry); }
    
    // Here we implement some fancy ordering logic
    // Later requests take over earlier ones
    void mergeBuffer();

  private:
    // The actual buffer
    std::deque<PrescaleEntry> m_contents;

  }; // end of class definition
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif
