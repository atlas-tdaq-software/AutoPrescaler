#ifndef __AutoPrescaler__PrescalerWrapper_base__
#define __AutoPrescaler__PrescalerWrapper_base__

#include <string>

// Run control related
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/OnlineServices.h"

#include <is/infodictionary.h>

#include <ers/ers.h>
#include "AutoPrescaler/prescaler/APLogger.h"

#include "DFdal/DFParameters.h"
#include "AutoPrescaler/prescaler/Prescaler.h"
#include "AutoPrescaler/ipc/PrescalerIpc.h"


class PrescalerWrapper : public daq::rc::Controllable
{
 public:
  PrescalerWrapper();
  ~PrescalerWrapper(){;}
  
  // Run control commands
  void configure(const daq::rc::TransitionCmd& ) override;
  void connect(const daq::rc::TransitionCmd& ) override {;} // nothing to be done
  void prepareForRun(const daq::rc::TransitionCmd& ) override; // activate timer
  void stopDC(const daq::rc::TransitionCmd& ) override {;} // nothing to be done
  void stopRecording(const daq::rc::TransitionCmd& ) override {;} // nothing to be done
  void stopArchiving(const daq::rc::TransitionCmd& ) override; // stop timer
  void unconfigure(const daq::rc::TransitionCmd& ) override {if(m_prescaler) delete m_prescaler; if(m_prescalerIpc) m_prescalerIpc->shutdown();} // cleanup
  void disconnect(const daq::rc::TransitionCmd& ) override{;} // nothing to be done here
  void publish() override; // this is where the watchdog functions are called

  // Commands to pass parameters
  //void passItems(std::string items) {m_items = items;}
  //void passRates(std::string rates) {m_rates = rates;}
  void passVerbose(unsigned int verbose) {m_verbose = verbose; APLoggerStatus::setLevel(verbose);}
  void passPartition(std::string partName) {m_partName = partName;}  
  // void performChecks();
  
 private:

  ISInfoDictionary* m_isDict;
  //std::string m_items;
  //std::string m_rates;
  std::string m_partName;
  unsigned int m_verbose;
  
  L1CT::Prescaler* m_prescaler;
  L1CT::PrescalerIpc* m_prescalerIpc;
};


#endif
