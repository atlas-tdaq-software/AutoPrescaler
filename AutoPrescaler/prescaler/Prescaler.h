#ifndef AutoPrescaler_prescaler_Prescaler_h
#define AutoPrescaler_prescaler_Prescaler_h
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/OnlineData.h>
#include <AutoPrescaler/prescaler/PrescaleBuffer.h>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include "rc/RunParams.h"
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------

// Steal the IS subscription implementation of OnlineData
  class Prescaler: public OnlineData {

  public:
    virtual ~Prescaler();
    Prescaler();

  private:

    virtual void callback(ISCallbackInfo * isc) final {
      // Handle the actual callback and notify others
      update(isc);
    }
    
    // override update function
    void update(ISCallbackInfo * isc);

    // Request a change in prescales
    bool requestPrescaleKey();

    // Publish history to IS
    void publishToIS();
    
  public:

    // Dedicated Subscription for Prescaler
    void subscribe();
    
    // Fast interface between rulechecker and prescaler
    // Buffer the requests for new prescales
    // Expected to be provided with the PSK for which this is valid
    bool bufferRequests(const std::vector<PrescaleBuffer::PrescaleEntry> &buffer);

    // Go through the entire buffer, merge all requests
    // Send a new pre-scale request
    bool processRequests();

    
  private:
    
    // Data-members

    // Check whether we are fully initialized
    bool m_init;

    // Prescale Key for which the last request was deemed to be valid
    unsigned int  m_validPSK;
    
    // List of prescale keys generated started from the original
    std::vector<unsigned int>  m_listPSK;

    // IS-read value
    CtpControllerL1PSKey  m_L1PSK;

    // Run-number from IS
    RunParams m_runParams;

    // Internal buffer
    PrescaleBuffer m_buffer;
    
    // Lock for IS callbacks
    mutable boost::recursive_mutex m_callbackLock;

    // Lock for key generation
    mutable boost::recursive_mutex m_prescaleLock;
    
  }; // end of class definition
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif
