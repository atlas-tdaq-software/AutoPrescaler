#ifndef AutoPrescaler_prescaler_APLogger_h
#define AutoPrescaler_prescaler_APLogger_h

#include <ers/ers.h>

/// may have to change to "info"
#ifndef AP_ERS_DEBUG
#define AP_ERS_DEBUG( message ) do { \
if ( APLoggerStatus::getLevel() == APLoggerStatus::LoggingLevel::DEBUG ) \
{ \
    ERS_REPORT_IMPL( ers::debug, ers::Message, message, ERS_EMPTY ); \
} } while(0)
#endif


// Class to set the logging level for the AP_* ers logs
// in simpler words - turn on and off the debug
class APLoggerStatus {
public:
    enum LoggingLevel {DEBUG = 2, INFO = 3};

    static void setLevel(APLoggerStatus::LoggingLevel level);
    static void setLevel(unsigned int level);
    static APLoggerStatus::LoggingLevel getLevel();

private:
    inline static LoggingLevel m_level = APLoggerStatus::LoggingLevel::INFO;
};


#endif
