#ifndef AutoPrescaler_rulechecker_DataModel_h
#define AutoPrescaler_rulechecker_DataModel_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/Subscriber.h>
#include <AutoPrescaler/rulechecker/OnlineData.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class DataModel : public Subscriber {

public:
    virtual ~DataModel() {}
    DataModel() : m_dataAbstraction(m_onlineData) {
        // Subscribe to updates of the DataAbstraction
        AP_ERS_DEBUG("Constructing DataModel");
        m_dataAbstraction.add(*this);
        AP_ERS_DEBUG("Added DataModel to the DataAbstraction callbacks");

        // Subscribe the online data to IS
        AP_ERS_DEBUG("Subscribing the onlineData");
        m_onlineData.subscribe();
    }

    inline const OnlineData &online() const { return m_onlineData; }
    inline const DataAbstraction &abstraction() const { return m_dataAbstraction; }
    // Non-const version for the moment: sloppy, to be fixed later
    inline DataAbstraction &abstraction() { return m_dataAbstraction; }

public: 
    virtual void callback(ISCallbackInfo * isc) final { 
        // Just debug. Print and call the parent.
        AP_ERS_DEBUG("Received IS callback from " << isc->name() << " type: " << isc->type());
        Subscriber::callback(isc);
    }

protected:
    OnlineData      m_onlineData; 
    DataAbstraction m_dataAbstraction;
};
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif

