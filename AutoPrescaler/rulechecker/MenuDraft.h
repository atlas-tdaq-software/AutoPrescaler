#ifndef AutoPrescaler_rulechecker_MenuDraft_h
#define AutoPrescaler_rulechecker_MenuDraft_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <map>
#include <vector>
#include <string>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/RuleCheckerChildren.h>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------

  class MenuDraft : public RuleCheckerChildren {

public:
    struct Entry {
        
        std::vector<float>   history;

        Entry &operator = (float v) { history.push_back(v); return *this; }
        
        operator float () const { return history.size() == 0 ? 0 : history[history.size()-1];}
    };

    typedef std::map<std::string, Entry> Draft_t;

public:
        
    // Map-like interface
    typedef Draft_t::iterator           iterator;
    typedef Draft_t::const_iterator     const_iterator;
    
    // Iterators and Accessors over MenuDraft
    iterator begin() { return m_currentDraft.begin(); }
    const_iterator begin() const { return m_currentDraft.begin(); }
    
    iterator end() { return m_currentDraft.end(); }
    const_iterator end() const { return m_currentDraft.end(); }

    size_t size() const { return m_currentDraft.size(); }

    Entry &operator [] (const std::string &n) { return m_currentDraft[n]; } 
    const Entry &operator [] (const std::string &n) const { return m_currentDraft.at(n); } 
  
public:

    // Clear draft
    bool reset (int currentL1PsKey);
    bool reset() { m_bufferedPrescales.clear(); m_currentDraft.clear(); return true; }
  
    // Check whether Draft has any new elements
    Draft_t check();

private:

    // For the moment, use a map for the draft, for lack of writing a dedicated class
    Draft_t m_currentDraft;

    int m_bufferL1PsKey;

    std::map<std::string, float> m_bufferedPrescales;

}; // end-of-class

} // namespace

#endif


