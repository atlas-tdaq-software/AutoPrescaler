#ifndef AutoPrescaler_rulechecker_PrescalerRule_h
#define AutoPrescaler_rulechecker_PrescalerRule_h
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/PrescalerAction.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/PrescalerCondition.h>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------

class PrescalerRule{

 private:

  // Condition associated to this rule
  PrescalerCondition* m_condition;

  // Action associated to this rule
  PrescalerAction* m_action;
  
  // In case we need more variety here
  enum State {Inactive, Active};
  
  // States
  State m_currentState;
  State m_lastState;
  

 public:
  
  // Placeholder constructors
  PrescalerRule() {}
 PrescalerRule(PrescalerCondition* conditionin, PrescalerAction* actionin): 
  m_condition(conditionin), m_action(actionin), m_currentState(State::Inactive), m_lastState(State::Inactive) {}

  // Destructor
  //~PrescalerRule() { if(m_action) delete m_action; if(m_condition) delete m_condition; }

  // Reset actions/conditions
  inline void reset() {m_action->reset(); m_condition->reset(); m_currentState = State::Inactive; m_lastState = State::Inactive;}
  
  // Execute the action associated to the current state
  bool callAction();

  // Update the actions: if the actions need to do some of their own book-keeping
  void updateAction() { m_action->update(); }

  // Update the conditions: move this directly to the RuleChecker a bit later
  void updateCondition() { m_condition->update(); }

  // Calculate the new state: sure, this is trivial.
  void update();

  std::vector<std::string> needsItems();

}; // end of class

} // namespace

#endif
