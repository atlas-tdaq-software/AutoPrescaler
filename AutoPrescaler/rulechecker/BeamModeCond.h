#ifndef AutoPrescaler_rulechecker_BeamModeCond_h
#define AutoPrescaler_rulechecker_BeamModeCond_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/PrescalerCondition.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>
#include <string>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------

// Specific Condition class for large average rates
class BeamModeCond : public PrescalerCondition {
    
  public:

    BeamModeCond() {reset();}
    
  BeamModeCond(std::string itemin, std::vector<std::string> beamModes):
    m_itemName(itemin), m_beamModes(beamModes) {reset();}
    
    // All functions to be overridden
    //    virtual ~BeamModeCond() override {}
    
    // Check if condition is fulfilled
    virtual bool truthValue() override;
    
    // Update the internal stores of this condition
    virtual bool update() override;

    // Reset all internal stores
    virtual void reset() override;

    // Print out information about this condition
    virtual void print() override;

    // Return the item we're using
    virtual std::vector<std::string> needsItems() override { 
      AP_ERS_DEBUG("BeamCondition needsItem: " << m_itemName);
      return std::vector<std::string>(1, m_itemName);
    }
    
  private:
    
    // Members set-up during initialization
    std::string m_itemName;
    std::vector<std::string> m_beamModes;
    float m_maxRate;
    int m_integrationTime;
    DataAbstraction::L1Item::Type m_rateType;

    // Internal book-keeping
    bool m_isHot;
    bool m_isCold;
    bool m_lastValue;
    bool m_timeElapsed;
    std::deque<std::pair<float, boost::posix_time::ptime> > m_rates;

  }; // end class

} // namespace
#endif
