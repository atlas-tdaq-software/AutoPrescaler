#ifndef AutoPrescaler_rulechecker_RuleChecker_h
#define AutoPrescaler_rulechecker_RuleChecker_h
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/Subscriber.h>
#include <AutoPrescaler/rulechecker/DataModel.h>
#include <AutoPrescaler/rulechecker/CombinedRule.h>
#include <AutoPrescaler/rulechecker/MenuDraft.h>
#include <AutoPrescaler/prescaler/Prescaler.h>
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------

class RuleChecker: public SubscriberInterface {

 private:

  // Are we properly initialized?
  bool m_init;

  // Pointer to Prescaler
  Prescaler* m_prescaler;

  // dataModel ownership
  DataModel m_dataModel;

  // Callback function from IS
  virtual void callback(ISCallbackInfo * isc) final {
    AP_ERS_DEBUG("Received IS callback from " << isc->name() << " type: " << isc->type());
    update(isc);
  };
  
  // Keep the rules via a vector
  std::vector<CombinedRule> m_rules;

  // Menu draft ownership
  MenuDraft m_menuDraft;

  // Need to hold in a field of the IS observables we need to retrieve
  // Separate format for rates and/or flags?

  // Local callback from IS
  bool update(ISCallbackInfo * isc);
  
  // Internal function to request a new pre-scale set based on the menu diff?
  // Probably actually pass-back a pre-scale key or a status
  bool requestPS(MenuDraft::Draft_t /*diffRequest*/);
  
  // Check if the histories are consistent
  bool checkPSKHistory();

  // Reset all rules/conditions/actions
  void globalReset();

  // Check if L1CT.CtpControllerLoadedL1PSKey, L1CT.CtpControllerScheduledL1PSKey and last item from L1CT.AutoPrescaler.PSKHistory are synchronized
  bool checkIfKeysInSync();

  // Print L1CT.CtpControllerLoadedL1PSKey, L1CT.CtpControllerScheduledL1PSKey and last item from L1CT.AutoPrescaler.PSKHistory
  std::string keysToString();

  // Book-keeping of L1 history
  std::vector<CtpControllerL1PSKey> m_lastPSKHistory;

  MenuDraft::Draft_t m_cachedRequest;
  
 public:

  // Constructor
  RuleChecker();
  virtual ~RuleChecker(){};

  // Public set-up accessor, for now
  void addRule(CombinedRule& rulein);
  
  // DataModel accessor
  inline const DataModel &dataModel() const { return m_dataModel; }
  
  // Draft Request
  inline void requestDraft(std::string itemName, float prescale) { m_menuDraft[itemName] = prescale;}

  // Attach Prescaler
  void attachPrescaler(Prescaler* myPrescaler) { m_prescaler = myPrescaler; }
  
}; // end-of-class

} // namespace

#endif
