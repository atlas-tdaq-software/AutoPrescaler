#ifndef AutoPrescaler_rulechecker_PrescalerAction_h
#define AutoPrescaler_rulechecker_PrescalerAction_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/RuleCheckerChildren.h>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <vector>
#include <string>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------

// interface baseclass for prescaler actions
  class PrescalerAction : public RuleCheckerChildren {
    
  public:
    
    // All functions to be overridden
    
    //virtual ~PrescalerAction() {}
    
    // Action when going from inactive to active
    virtual bool activate() { return true; }
    
    // Action when going from active to inactive
    virtual bool deactivate() { return true; }
    
    // Action when going from active to active
    virtual bool repeatActivate() { return true; }
    
    // Action when going from inactive to inactive
    virtual bool repeatDeactivate() { return true; }
    
    // Update all internally calculated quantities
    virtual bool update() { return true; }

    // Wipe-out all internally calculated quantities
    virtual void reset() {}

    // Print out information about this condition
    virtual void print() {}
    
    // Tells the abstraction what we need to keep track of
    virtual std::vector<std::string> needsItems() {return std::vector<std::string>();}
    
}; // end class

} // namespace
#endif
