#ifndef AutoPrescaler_rulechecker_OnlineData_h
#define AutoPrescaler_rulechecker_OnlineData_h
//------------------------------------------------------------------------------

#include <AutoPrescaler/prescaler/APLogger.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/Subscriber.h>
#include <AutoPrescaler/rulechecker/OnlineData.h>
//------------------------------------------------------------------------------
#include <TTCInfo/CtpcoreTriggerRateInfo.h>
#include <TTCInfo/CtpControllerL1PSKey.h>
#include <TTCInfo/CtpControllerL1PSKeyList.h>
#include <ddc/DdcStringInfo.h>
#include <ipc/partition.h>
#include <is/infodictionary.h>

//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class OnlineData : public Subscriber {

public:
    virtual ~OnlineData();
    OnlineData();

public:
    void subscribe();
    void unsubscribe();


protected:
    // IS subscription interfaces
    bool                m_subscribed;
    ISInfoReceiver      m_initialReceiver; // Initial partition (i.e., initialL1CT)
    ISInfoReceiver      m_primaryReceiver; // Primary partition (i.e., ATLAS)
    
    // Subscribe using pattern
    void subscribeTo(ISInfoReceiver &receiver, const std::string &server, const ISCriteria &criteria);
    
    // Helpers
    inline void subscribeTo(ISInfoReceiver &receiver, const std::string &server, const std::string &pattern) { 
        subscribeTo(receiver, server, ISCriteria(pattern));
    }
    // More helpers
    template <typename T>
    void subscribeToPrimary(const std::string &server, const T &cond) { return subscribeTo(m_primaryReceiver, server, cond); }

    template <typename T>
    void subscribeToInitial(const std::string &server, const T &cond) { return subscribeTo(m_initialReceiver, server, cond); }

public:
    // Add getters available to others here
    inline const CtpcoreTriggerRateInfo &triggerRates() const { return m_triggerRates; }
    inline const CtpControllerL1PSKey  &triggerL1PSK() const { return m_L1PSK; }
    inline const CtpControllerL1PSKey  &triggerScheduledL1PSK() const { return m_scheduledL1PSK; }
    inline const CtpControllerL1PSKeyList  &triggerPSKHistory() const { return m_PSKHistory; }
    inline const ddc::DdcStringInfo &beamMode() const { return m_beamMode; }

protected:
    // Add screened/private methods and data members here

    // Generic approach to storing ISInfo by name
    typedef std::map<std::string, ISInfo*> infomap_t;
    infomap_t m_infoSubscribed;

    // Add specific objects we need to keep track of
    CtpcoreTriggerRateInfo   m_triggerRates;
    CtpControllerL1PSKey     m_L1PSK;
    CtpControllerL1PSKey     m_scheduledL1PSK;
    CtpControllerL1PSKeyList m_PSKHistory;
    ddc::DdcStringInfo       m_beamMode;
    
protected:
    virtual void callback(ISCallbackInfo * isc) override { 

      // Handle the actual callback and notify others
      update(isc);
      AP_ERS_DEBUG("Notifying " << m_clients.size() << " clients");
      notify(isc);
    }

    // Assume this is your callback function
    void update(ISCallbackInfo * isc);

};
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif

