#ifndef AutoPrescaler_rulechecker_AvgRateCond_h
#define AutoPrescaler_rulechecker_AvgRateCond_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/PrescalerCondition.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>
#include <string>
//------------------------------------------------------------------------------
#include <boost/date_time/posix_time/posix_time.hpp>
//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------

// Specific Condition class for large average rates
class AvgRateCond : public PrescalerCondition {
    
  public:

    AvgRateCond() {reset();}
    
  AvgRateCond(std::string itemin, float maxratein, int timein, DataAbstraction::L1Item::Type type):
    m_itemName(itemin), m_maxRate(maxratein), m_integrationTime(timein), m_rateType(type) {reset();}
    
    // All functions to be overridden
    //    virtual ~AvgRateCond() override {}
    
    // Check if condition is fulfilled
    virtual bool truthValue() override;
    
    // Update the internal stores of this condition
    virtual bool update() override;

    // Reset all internal stores
    virtual void reset() override;

    // Print out information about this condition
    virtual void print() override;

    // Return the item we're using
    virtual std::vector<std::string> needsItems() override { 
      ERS_LOG("Condition needsItem: ");
return std::vector<std::string>(1, m_itemName);}
    
  private:
    
    // Members set-up during initialization
    std::string m_itemName;
    float m_maxRate;
    int m_integrationTime;
    DataAbstraction::L1Item::Type m_rateType;

    // Internal book-keeping
    bool m_isHot;
    bool m_isCold;
    bool m_lastValue;
    bool m_timeElapsed;
    std::deque<std::pair<float, boost::posix_time::ptime> > m_rates;

  }; // end class

} // namespace
#endif
