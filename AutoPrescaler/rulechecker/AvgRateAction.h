#ifndef AutoPrescaler_rulechecker_AvgRateAction_h
#define AutoPrescaler_rulechecker_AvgRateAction_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/PrescalerAction.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>
//------------------------------------------------------------------------------
#include <boost/date_time/posix_time/posix_time.hpp>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <string>
#include <queue>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------

// interface baseclass for prescaler actions
class AvgRateAction: public PrescalerAction {
    
 public:
  
  // All functions to be overridden
    AvgRateAction() {}
  AvgRateAction(std::string itemin, float ratein, float safetyin, int roundin, int timein, DataAbstraction::L1Item::Type type):
    m_itemName(itemin), m_targetRate(ratein), m_safetyFactor(safetyin), m_roundingFactor(roundin), m_integrationTime(timein), m_rateType(type) {}
    
    virtual ~AvgRateAction() {}
    
  // Action when going from inactive to active
  virtual bool activate() override;
  
  // Action when going from active to active
  virtual bool repeatActivate() override;
  
  // Action when going from inactive to active
  virtual bool deactivate() override;

  // Update all internally calculated quantities
  virtual bool update() override;

  // Reset internal stores
  virtual void reset() override;
  
  // Print relevant information
  virtual void print() override;

  // return the trigger(s) we monitor
  virtual std::vector<std::string> needsItems() override {return std::vector<std::string>(1, m_itemName);}

  private:

  // Calculate prescale as an nth power
  // Can eventually switch to a sort of % resolution
  int powerOf(float targetPrescale);

  // Members set-up during initialization
  std::string m_itemName;
  float m_targetRate;
  float m_safetyFactor;
  int m_roundingFactor;
  int m_integrationTime;
  DataAbstraction::L1Item::Type m_rateType;

  // Internal book-keeping
  std::deque<std::pair<float, boost::posix_time::ptime> > m_rates;
  unsigned int m_requestedPS;
  double m_ratioPS;
  //  OWLTime m_lastHot;
  boost::posix_time::ptime  m_lastHot;

}; // end class

} // namespace
#endif
