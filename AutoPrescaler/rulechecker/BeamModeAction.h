#ifndef AutoPrescaler_rulechecker_BeamModeAction_h
#define AutoPrescaler_rulechecker_BeamModeAction_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/PrescalerAction.h>
#include <AutoPrescaler/rulechecker/DataAbstraction.h>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <string>
#include <queue>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------

// interface baseclass for prescaler actions
class BeamModeAction: public PrescalerAction {
    
 public:
  
  // All functions to be overridden
  BeamModeAction() {reset();}

  BeamModeAction(std::string itemin, int targetPrescale):
  m_itemName(itemin), m_targetPrescale(targetPrescale) {reset();}
    
  virtual ~BeamModeAction() {}
    
  // Action when going from inactive to active
  virtual bool activate() override;
  
  // Action when going from active to active
  virtual bool repeatActivate() override;
  
  // Action when going from inactive to active
  virtual bool deactivate() override;

  // Update all internally calculated quantities
  virtual bool update() override;

  // Reset internal stores
  virtual void reset() override;
  
  // Print relevant information
  virtual void print() override;

  // return the trigger(s) we monitor
  virtual std::vector<std::string> needsItems() override {return std::vector<std::string>(1, m_itemName);}

  private:

  // Members set-up during initialization
  std::string m_itemName;
  
  // Target prescale
  int m_targetPrescale;

  // Request to revert to original PS
  float m_originalPS;
  

}; // end class

} // namespace
#endif
