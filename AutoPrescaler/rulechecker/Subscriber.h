#ifndef AutoPrescaler_rulechecker_Subscriber_h
#define AutoPrescaler_rulechecker_Subscriber_h
//------------------------------------------------------------------------------
#include <is/inforeceiver.h>
//------------------------------------------------------------------------------
#include <boost/thread/recursive_mutex.hpp>
//------------------------------------------------------------------------------
#include <set>


//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class SubscriberInterface {
    // Interface class for anything that can receive an IS callback

public:
    // Signature of IS callback
    virtual void callback(ISCallbackInfo * isc) = 0;

};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class Subscriber : public SubscriberInterface {
    // Interface to distribute callbacks downstream

public:
    void add(SubscriberInterface &s) { m_clients.insert(&s); }
    void remove(SubscriberInterface &s) { m_clients.erase(&s); }

protected:
    virtual void callback(ISCallbackInfo * isc) override { return notify(isc); };

    void notify(ISCallbackInfo * isc) {
        boost::recursive_mutex::scoped_lock lock(m_mutex);

        for(auto client : m_clients) client->callback(isc);
    }

    mutable boost::recursive_mutex      m_mutex;
    
    // Should be private, but remove the protection for testing...
    std::set<SubscriberInterface * >    m_clients;
};
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif

