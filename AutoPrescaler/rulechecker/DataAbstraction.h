#ifndef AutoPrescaler_rulechecker_DataAbstraction_h
#define AutoPrescaler_rulechecker_DataAbstraction_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/Subscriber.h>
#include <AutoPrescaler/rulechecker/OnlineData.h>
//------------------------------------------------------------------------------
#include "ers/ers.h"
#include <AutoPrescaler/prescaler/APLogger.h>
#include <set>
#include <map>

//------------------------------------------------------------------------------
namespace L1CT {
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class DataAbstraction : public Subscriber {

public:
    virtual ~DataAbstraction();
    DataAbstraction(OnlineData &online);

    inline const OnlineData &online() const { return m_online; }
    
    // Re-formatting of the ISInformation, with some filtering
    struct L1Item {
      bool enabled;
      float prescale;
      float originalprescale;
      float rateTBP;
      float rateTAP;
      float rateTAV;
      
      enum Type{TBP, TOP, TAP, TAV};
      
      L1Item(bool enabledin, float prescalein, float originalprescalein, float rateTBPin, float rateTAPin, float rateTAVin) :
      enabled(enabledin), prescale(prescalein), originalprescale(originalprescalein), rateTBP(rateTBPin), rateTAP(rateTAPin), rateTAV(rateTAVin) { return; }
      
      L1Item() { return; }
      L1Item(const L1Item& itemin) :
      enabled(itemin.enabled), prescale(itemin.prescale), originalprescale(itemin.originalprescale), rateTBP(itemin.rateTBP), rateTAP(itemin.rateTAP), rateTAV(itemin.rateTAV) { return; }
      
    };

protected:
    virtual void callback(ISCallbackInfo * isc) final { 
      AP_ERS_DEBUG("Received IS callback from " << isc->name() << " type: " << isc->type());
      // Handle the actual callback and notify others
      update(isc);
      notify(isc);
    }

    // Assume this is your callback function
    void update(ISCallbackInfo * isc);

    // Container for all the info we read from the L1Rates
    std::map<std::string, L1Item> m_L1ItemsDetails;
    std::set<std::string> m_L1ItemsMonitored;

  public:
    // Add getters available to others here
    float getL1Rate(std::string trigName, L1Item::Type rateType) const; 
    inline float getL1Prescale(std::string trigName) const { return m_L1ItemsDetails.at(trigName).prescale; }
    inline bool  checkItem(std::string trigName) const { return m_L1ItemsDetails.find(trigName) == m_L1ItemsDetails.end() ? false: true; }
    inline bool  isEnabled(std::string trigName) const { return m_L1ItemsDetails.at(trigName).enabled; }
    
    // Keep track of a specific item
    inline void addL1Item(std::string trigName) { 
      AP_ERS_DEBUG("Adding L1 item to abstraction " << trigName);
      m_L1ItemsMonitored.insert(trigName); 
    }

    // Retrieve L1 PSK
    inline unsigned int getL1PSK() const { return online().triggerL1PSK().L1PrescaleKey; }
    inline unsigned int getScheduledL1PSK() const { return online().triggerScheduledL1PSK().L1PrescaleKey; }

    // Retrieve Beam Mode
    inline std::string getBeamMode() const { return std::string(online().beamMode().value); }
    

protected:
    // Add screened/private methods and data members here
    
private:
    const OnlineData &  m_online;

};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
#endif

