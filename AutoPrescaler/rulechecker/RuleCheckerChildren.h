#ifndef AutoPrescaler_rulechecker_RuleCheckerChildren_h
#define AutoPrescaler_rulechecker_RuleCheckerChildren_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------

class RuleChecker;

// interface baseclass for prescaler actions
 class RuleCheckerChildren{
   
 public:
   
   RuleCheckerChildren() {}
   void setRuleChecker(RuleChecker& myRule) {m_ruleChecker=&myRule;}

 protected:   
   RuleChecker& rulechecker() {return *m_ruleChecker;}
   
 private:
   RuleChecker* m_ruleChecker;
    
}; // end class

} // namespace
#endif
