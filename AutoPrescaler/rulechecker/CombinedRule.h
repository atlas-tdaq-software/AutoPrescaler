#ifndef AutoPrescaler_rulechecker_CombinedRule_h
#define AutoPrescaler_rulechecker_CombinedRule_h
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/RuleCheckerChildren.h>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <vector>
#include <queue>
//------------------------------------------------------------------------------
#include <ers/ers.h>
#include <AutoPrescaler/prescaler/APLogger.h>

#include <boost/date_time.hpp>

//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// New implementation of the RuleClass for 2016:
// This class handles the combination between prescale requests on items going
// hot and items being un-prescaled to handle beamMode transitions
// 
// Since there is now communication between the decision and action, this
// usecase can now be handled nearly trivially
//------------------------------------------------------------------------------

class CombinedRule: public RuleCheckerChildren {

 private:

    // Members set-up during initialization
    std::string m_itemName;
    std::vector<std::string> m_beamModes;
    float m_maxRate;
    float m_targetRate;
    int m_integrationTime;
    int m_roundingFactor;

    // Internal book-keeping
    bool m_timeElapsed;
    float m_originalPS;
    boost::posix_time::ptime m_lastHot; 
    bool m_newPS;
    
    // Rate history
    std::deque<std::pair<float, boost::posix_time::ptime> > m_prescaledRates;
    std::deque<std::pair<float, boost::posix_time::ptime> > m_unprescaledRates;
    std::deque<std::pair<float, boost::posix_time::ptime> > m_originalRates;
    std::deque<std::pair<int, boost::posix_time::ptime> > m_prescales;
    
    // States: Hot && Beam
    bool m_isHot;
    bool m_isBeam;

    // Initialization of originalPS
    bool m_initialized;
    
 public:
  
  // Placeholder constructors
  CombinedRule() {}

  // To review later
  CombinedRule(std::string itemName, std::vector<std::string> beamModes, float maxRate, float targetRate, int integrationTime, int roundingFactor):
    m_itemName(itemName), 
    m_beamModes(beamModes), 
    m_maxRate(maxRate), 
    m_targetRate(targetRate), 
    m_integrationTime(integrationTime), 
    m_roundingFactor(roundingFactor) { reset(); }

  // Reset actions/conditions
  void reset() {
    // Calling a reset
    //ERS_LOG("Clearing all internal stores for rule " << m_itemName << " target rate " << m_targetRate);

    m_isHot = false;
    m_isBeam = false;
    m_initialized = false;
    m_timeElapsed = false;

    m_newPS = false;

    m_originalPS = 0;

    m_originalRates.clear();
    m_unprescaledRates.clear();
    m_prescaledRates.clear();
    m_prescales.clear();
  }
  
  // Prepare a new prescale request (if needed)
  bool request();

  // Update internal stores with current psk and current rates also checks for the psk value change
  bool update();

  // List of needed items: mostly for historical reasons
  std::vector<std::string> needsItems();

  // Calculate prescale as an nth power
  int powerOf(float targetPrescale);


  // Return the name of the rule, used for printouts
  const std::string& name() const {return m_itemName;}

}; // end of class

} // namespace

#endif
