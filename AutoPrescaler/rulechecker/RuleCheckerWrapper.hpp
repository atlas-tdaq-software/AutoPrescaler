//------------------------------------------------------------------------------
//
// IMPORTANT: 
//
//  The order of functions in this file matters. Specializations MUST 
//      A) be defined and implemented before their first use and 
//      B) be defined/implemented before the full template at the end. 
//
//      This means that the order should be something like:
//
//      1) Specializations for different rule types 
//      2) Specialization for base class
//      3) Full template as a catch all (if so desired)
//
//
//------------------------------------------------------------------------------
#include <APRules/AllowedBeamMode.h>
#include <APRules/HotItemRule.h>
#include <APRules/BeamModeRule.h>
#include <APRules/CombinedRule.h>
//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/AvgRateCond.h>
#include <AutoPrescaler/rulechecker/AvgRateAction.h>
#include <AutoPrescaler/rulechecker/BeamModeCond.h>
#include <AutoPrescaler/rulechecker/BeamModeAction.h>
#include <AutoPrescaler/rulechecker/CombinedRule.h>

//------------------------------------------------------------------------------
// PART 1: Specializations
//------------------------------------------------------------------------------
template <>
bool RuleCheckerWrapper::addRule(const APRules::HotItemRule * const rule) {

  ERS_INFO(" Creating HotItem Condition...");
  const auto name             = rule->get_ItemName();
  const auto timeWindow       = rule->get_TimeWindow();
  const auto maxRate          = rule->get_MaxRate();
  const auto safetyFactors    = rule->get_SafetyFactor();
  const auto roundingFactors  = rule->get_RoundingFactor();
  
  ERS_INFO(name);
  ERS_INFO(maxRate);
  ERS_INFO(timeWindow);

  L1CT::AvgRateCond * condition = new L1CT::AvgRateCond(name, maxRate, timeWindow, L1CT::DataAbstraction::L1Item::Type::TOP);
  ERS_INFO(" Creating HotItem Action...");
  ERS_INFO(safetyFactors);
  ERS_INFO(roundingFactors); 

  L1CT::AvgRateAction * action    = new L1CT::AvgRateAction(name, maxRate, safetyFactors, roundingFactors, timeWindow,  L1CT::DataAbstraction::L1Item::Type::TBP);
  
  bootstrap(condition, action);

  return true;
}
//------------------------------------------------------------------------------
template <>
bool RuleCheckerWrapper::addRule(const APRules::BeamModeRule * const rule) {
  ERS_INFO("Creating BeamMode Condition...");

  const std::string name        = rule->get_ItemName();
  const uint32_t targetPrescale = rule->get_TargetPrescale();
  const std::vector<const APRules::AllowedBeamMode*>& beamModes = rule->get_BeamModes();

  ERS_INFO("The name of new BeamModeRule is " << name);

  std::vector<std::string> beamModesNames;
  for(const APRules::AllowedBeamMode* mode : beamModes) {
    beamModesNames.push_back(mode->get_ModeName());
  }

  auto * condition = new L1CT::BeamModeCond(name, beamModesNames);
  ERS_INFO("Creating BeamMode Action...");   

  auto * action = new L1CT::BeamModeAction(name, targetPrescale);
  for(const std::string& mode : beamModesNames){
    ERS_INFO("    with beamMode: " << mode);
  }

  ERS_INFO("    and target prescale: " << targetPrescale);

  bootstrap(condition, action);

  return true;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template <>
bool RuleCheckerWrapper::addRule(const APRules::CombinedRule * const rule) {
  ERS_INFO(" Creating CombinedRule...");
  const std::string name          = rule->get_ItemName();
  const uint32_t timeWindow       = rule->get_TimeWindow();
  const uint32_t maxRate          = rule->get_MaxRate();
  const uint32_t targetRate       = rule->get_TargetRate();
  const uint32_t roundingFactors  = rule->get_RoundingFactor();
  const std::vector<const APRules::AllowedBeamMode*>& beamModes = rule->get_BeamModes();

  std::vector<std::string> beamModesNames;
  for(const APRules::AllowedBeamMode* mode : beamModes) {
    beamModesNames.push_back(mode->get_ModeName());
  }

  ERS_INFO("The name of new CombinedRule is " << name);
  auto * myRule = new L1CT::CombinedRule(name, beamModesNames, maxRate, targetRate, timeWindow, roundingFactors);
  
  for(const std::string& mode : beamModesNames){
    ERS_INFO("  with beamMode: " << mode);
  }

  ERS_INFO("  itemName: " << name);
  ERS_INFO("  maxRate: " << maxRate);
  ERS_INFO("  targetRate: " << targetRate);
  ERS_INFO("  integrationTime: " << timeWindow);
  ERS_INFO("  rounding by: " << roundingFactors);

  bootstrap(myRule);

  return true;
}
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// PART 2: Specialization for base class
//------------------------------------------------------------------------------
#define addRuleAs(X, Y)							\
  do {									\
    const Y * const __ptr = X->cast<Y>();                               \
    AP_ERS_DEBUG(" Try casting...");                                         \
    if(__ptr == nullptr) break;						\
    AP_ERS_DEBUG(" Casting successful...");                                  \
    return addRule(__ptr);						\
  } while (false)

template <>
bool RuleCheckerWrapper::addRule(const APRules::AutoPrescalerRule * const rule) {
  // Add a line here for each new type defined above
  AP_ERS_DEBUG("Trying to add Rule of class " << rule->s_class_name);

  AP_ERS_DEBUG("Trying Rule as a combined rule...");
  addRuleAs(rule, APRules::CombinedRule);
  
  AP_ERS_DEBUG("Trying Rule as a HotItem rule...");
  addRuleAs(rule, APRules::HotItemRule);

  AP_ERS_DEBUG("Trying Rule as a BeamMode rule...");
  addRuleAs(rule, APRules::BeamModeRule);

  AP_ERS_DEBUG("Neither of them works - give up...");
  return false;
}
#undef addRuleAs
//------------------------------------------------------------------------------
// PART 3: Full Template
//------------------------------------------------------------------------------
//template <typename T>
//bool RuleCheckerWrapper::addRule(const T * const rule) {
//    // Gracefully catch/handle non-implemented classes
//    ERS_ERROR("OKS type" << rule->class_name() << "is not supported!");
//    return false;
//}
//------------------------------------------------------------------------------
