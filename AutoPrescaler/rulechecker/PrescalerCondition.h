#ifndef AutoPrescaler_rulechecker_PrescalerCondition_h
#define AutoPrescaler_rulechecker_PrescalerCondition_h
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
#include <AutoPrescaler/rulechecker/RuleCheckerChildren.h>
//------------------------------------------------------------------------------
#include <vector>
#include <string>
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
namespace L1CT{
//------------------------------------------------------------------------------

// interface baseclass for prescaler actions
  class PrescalerCondition: public RuleCheckerChildren {

 public:

  
  // All functions to be overridden
    // PrescalerCondition() : L1CTLogger("PrescalerCondition") {}
    //virtual ~PrescalerCondition() {}

  // Check if condition is fulfilled
  virtual bool truthValue() { return true; }

  // Update the internal stores of this condition
  virtual bool update() { return true; }

  // Clean internal stores for book-keeping
  virtual void reset() {}

  // Print out information about this condition
  virtual void print() {}

  // Tells the abstraction what we need to keep track of
  virtual std::vector<std::string> needsItems() {return std::vector<std::string>();}

}; // end class

} // namespace
#endif
