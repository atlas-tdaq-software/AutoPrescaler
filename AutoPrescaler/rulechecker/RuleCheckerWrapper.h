#ifndef __AutoPrescaler__RuleCheckerWrapper_base__
#define __AutoPrescaler__RuleCheckerWrapper_base__

#include <string>

// Run control related
#include <RunControl/Common/Controllable.h>

#include <ers/ers.h>
#include <AutoPrescaler/prescaler/APLogger.h>

#include <AutoPrescaler/rulechecker/RuleChecker.h>
#include <AutoPrescaler/rulechecker/PrescalerCondition.h>
#include <AutoPrescaler/rulechecker/PrescalerAction.h>
#include <AutoPrescaler/rulechecker/CombinedRule.h>


class RuleCheckerWrapper : public daq::rc::Controllable
{
 public:
  RuleCheckerWrapper();
  ~RuleCheckerWrapper(){;}
  
  // Run control commands
  void configure(const daq::rc::TransitionCmd& ) override;
  void connect(const daq::rc::TransitionCmd& ) override {;} // nothing to be done
  void prepareForRun(const daq::rc::TransitionCmd& ) override; // activate timer
  void stopDC(const daq::rc::TransitionCmd& ) override {;} // nothing to be done
  void stopRecording(const daq::rc::TransitionCmd& ) override {;} // nothing to be done
  void stopArchiving(const daq::rc::TransitionCmd& ) override; // stop timer
  void unconfigure(const daq::rc::TransitionCmd& ) override {if(m_rulechecker) delete m_rulechecker;} // cleanup
  void disconnect(const daq::rc::TransitionCmd& ) override{;} // nothing to be done here
  void publish() override; // this is where the watchdog functions are called

  // Commands to pass parameters
  void passItems(std::string items) {m_items = items;}
  void passRates(std::string rates) {m_rates = rates;}
  void passVerbose(unsigned int verbose) {m_verbose = verbose; APLoggerStatus::setLevel(verbose);}
  void passPartition(std::string partName) {m_partName = partName;}
  void passBeamItems(std::string items) {m_beamItems = items;}


 protected:

  // This is defined in the .hpp
  template <typename T>
  bool addRule(const T * const rule);
  
  void bootstrap(L1CT::PrescalerCondition *c, L1CT::PrescalerAction *a) {
    // Keep this function for historical reasons: it is now deprecated
    // well, it is still used....

    // Assign RuleChecker for access to monitored quantities
    c->setRuleChecker(*m_rulechecker);
    a->setRuleChecker(*m_rulechecker);

    // Create rule
    //auto * r = new L1CT::PrescalerRule(c, a);

    // Add rule to rulechecker - this functionality does not work anymore
    //m_rulechecker->addRule(*r);
  }
  
  void bootstrap(L1CT::CombinedRule *r) {
    // Assign RuleChecker for access to monitored quantities
    r->setRuleChecker(*m_rulechecker);

    // Add rule to rulechecker
    m_rulechecker->addRule(*r);
  }

 private:
  
  std::string m_items;
  std::string m_beamItems;
  std::string m_rates;
  std::string m_partName;
  unsigned int m_verbose;

  L1CT::RuleChecker* m_rulechecker;
  
};


#endif
